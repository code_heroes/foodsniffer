package com.trustfood.trustfood.writers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by razir on 06.11.2014.
 * The Measure Interval is the characteristic where the user can set the time interval in msec,
 * that the reader count between the Measures Avg
 */
public class MeasurementIntervalWriter implements ValueWriter {

    List<ValueWriterResolver> values=new ArrayList<>();

    public void init() {
        ValueWriterResolver resolver=new ValueWriterResolver("Interval","",true);
        resolver.minBytes=2;
        values.add(resolver);
    }

    @Override
    public List<ValueWriterResolver> getValues() {
        return values;
    }
}
