package com.trustfood.trustfood.writers;

/**
 * Created by razir on 07.11.2014.
 */
public class WritersManager {

    public static final String COMMAND="Command";
    public static final String PUMP1="Pump1";
    public static final String PUMP2="Pump2";
    public static final String MeasurementNo="Measurement No";
    public static final String MeasurementAvg="Measures Avg";
    public static final String MeasurementInterval="Measure Interval";
    public static final String DelayBefore="Delay Before";
    public static final String DelayAfter="Delay After";
    public static final String CurrentValue="Current Value";


    public static ValueWriter getWriter(String charName){
        ValueWriter valueWriter=null;
        switch (charName){
            case COMMAND:
                valueWriter=new CommandWriter();
                break;
            case PUMP1:
                valueWriter=new PumpWriter();
                break;
            case PUMP2:
                valueWriter=new PumpWriter();
                break;
            case MeasurementNo:
                valueWriter=new MeasurementNoWriter();
                break;
            case MeasurementAvg:
                valueWriter=new MeasurementAvgWriter();
                break;
            case MeasurementInterval:
                valueWriter=new MeasurementIntervalWriter();
                break;
            case DelayBefore:
                valueWriter=new DelayBeforeWriter();
                break;
            case DelayAfter:
                valueWriter=new DelayAfterWriter();
                break;
            case CurrentValue:
                valueWriter=new CurrentValueWriter();
                break;
        }
        if(valueWriter!=null){
            valueWriter.init();
        }
        return valueWriter;
    }
}
