package com.trustfood.trustfood.writers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dennis on 13-05-2016
 * The Delay Before is the field where the user can set the delay time in msec,
 * that the reader counts before performing the Measures Avg
 */
public class DelayBeforeWriter implements ValueWriter {

    List<ValueWriterResolver> values=new ArrayList<>();

    public void init() {
        ValueWriterResolver resolver=new ValueWriterResolver("DelayBefore","",true);
        resolver.minBytes=2;
        values.add(resolver);
    }

    @Override
    public List<ValueWriterResolver> getValues() {
        return values;
    }
}
