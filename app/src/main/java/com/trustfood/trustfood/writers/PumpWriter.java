package com.trustfood.trustfood.writers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by razir on 06.11.2014.
 */
public class PumpWriter implements ValueWriter {

    List<ValueWriterResolver> values=new ArrayList<>();

    public void init() {
        values.add(new ValueWriterResolver("Forward","6A",true));
        values.add(new ValueWriterResolver("Backward","6B",true));
        values.add(new ValueWriterResolver("StandBy","6C",true));
        values.add(new ValueWriterResolver("Brake","6D",true));
    }

    @Override
    public List<ValueWriterResolver> getValues() {
        return values;
    }
}
