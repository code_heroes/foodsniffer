package com.trustfood.trustfood.writers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dennis on 13-05-2016
 * Measures average is the characteristic where the user can define
 * the number of the measurements that the reader will read and average
 */
public class MeasurementAvgWriter implements ValueWriter {

    List<ValueWriterResolver> values=new ArrayList<>();

    public void init() {
        ValueWriterResolver resolver=new ValueWriterResolver("Avg","",true);
        resolver.minBytes=1;
        values.add(resolver);
    }

    @Override
    public List<ValueWriterResolver> getValues() {
        return values;
    }
}
