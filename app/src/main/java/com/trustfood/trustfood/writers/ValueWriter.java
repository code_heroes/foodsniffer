package com.trustfood.trustfood.writers;

import com.trustfood.trustfood.writers.ValueWriterResolver;

import java.util.List;

/**
 * Created by razir on 06.11.2014.
 */
public interface ValueWriter {
    public void init();
    public List<ValueWriterResolver> getValues();
}
