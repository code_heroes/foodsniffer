package com.trustfood.trustfood.writers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by razir on 06.11.2014.
 */
public class MeasurementNoWriter implements ValueWriter {

    List<ValueWriterResolver> values=new ArrayList<>();

    public void init() {
        ValueWriterResolver resolver=new ValueWriterResolver("Number","",true);
        resolver.minBytes=4;
        values.add(resolver);
    }

    @Override
    public List<ValueWriterResolver> getValues() {
        return values;
    }
}
