package com.trustfood.trustfood.writers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by razir on 06.11.2014.
 */
public class CommandWriter implements ValueWriter {

    List<ValueWriterResolver> values=new ArrayList<>();

    public void init() {
        values.add(new ValueWriterResolver("Reset","F8"));
        values.add(new ValueWriterResolver("StartMeasure","A1"));
        values.add(new ValueWriterResolver("StopMeasure","A2"));
        values.add(new ValueWriterResolver("OnlyTeHu","A4"));
        values.add(new ValueWriterResolver("GetRSSI","11"));
        values.add(new ValueWriterResolver("DelBonds","1F"));
        values.add(new ValueWriterResolver("SetPump1","61"));
        values.add(new ValueWriterResolver("SetPump2","62"));
        values.add(new ValueWriterResolver("SetPumps","63"));

//        Map<String,String> leds=new HashMap<>();
//        for(int i=0;i<10;++i) {
//            leds.put("Led"+i, String.valueOf(i));
//        }
//        leds.put("All","F");
//        values.add(new ValueWriterResolver("onLedX","B",leds));
//        values.add(new ValueWriterResolver("offLedX","D",leds));

    }

    @Override
    public List<ValueWriterResolver> getValues() {
        return values;
    }
}
