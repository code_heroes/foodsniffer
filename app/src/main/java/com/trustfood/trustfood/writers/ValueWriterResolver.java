package com.trustfood.trustfood.writers;


import com.trustfood.trustfood.utils.Hex;

import org.apache.log4j.Logger;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by razir on 06.11.2014.
 */
public class ValueWriterResolver {
    private final Logger log = Logger.getLogger(ValueWriterResolver.class.getSimpleName());

    String name;
    String startByte;
    Map<String, String> options = new HashMap<>();
    boolean anyValue;
    int minBytes=1;

    public ValueWriterResolver(String name, String startByte) {
        this.name = name;
        this.startByte = startByte;
    }

    public ValueWriterResolver(String name, String startByte, boolean anyValue) {
        this.name = name;
        this.startByte = startByte;
        this.anyValue = anyValue;
    }

    public ValueWriterResolver(String name, String startByte, Map<String, String> options) {
        this.name = name;
        this.startByte = startByte;
        this.options = options;
    }

    public int getMinBytes() {
        return minBytes;
    }

    public void setMinBytes(int minBytes) {
        this.minBytes = minBytes;
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getOptions() {
        return options;
    }

    public boolean isAnyValue() {
        return anyValue;
    }

    public boolean hasOptions() {
        if (options != null && !options.isEmpty()) {
            return true;
        }
        return false;
    }

    public static byte[] hexStringToByteArray(String s, int minBytesCount) {
        byte[] data = null;
        try {
            data = Hex.decodeHex(s.toCharArray());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
//        int len = s.length();
//        byte[] data = new byte[len / 2];
//        for (int i = 0; i < len; i += 2) {
//            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
//                    + Character.digit(s.charAt(i+1), 16));
//        }
        if (data.length < minBytesCount) {
            int blankBytes = minBytesCount - data.length;
            byte[] result = new byte[minBytesCount];
            for (int i = 0; i < data.length; ++i) {
                result[blankBytes + i - 1] = data[i];
            }
            return result;
        }
        return data;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public byte[] getValue(String key) {

        String value = startByte + options.get(key);
        log.debug(key);
        return hexStringToByteArray(value);
    }

    public byte[] getValue(Integer integer) {
        byte[] bytes =null;
        if(minBytes<=2){
            bytes = ByteBuffer.allocate(minBytes).putShort(integer.shortValue()).array();
        }
        else {
            bytes = ByteBuffer.allocate(minBytes).putInt(integer.intValue()).array();
        }

        String value = Hex.encodeHexString(bytes);
        String result = startByte + value;
        log.debug(result);
        return hexStringToByteArray(result, minBytes);
    }

    public byte[] getValue() {
        log.debug(startByte);
        return hexStringToByteArray(startByte);
    }

    @Override
    public String toString() {
        return name;
    }
}
