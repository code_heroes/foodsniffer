package com.trustfood.trustfood;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.trustfood.trustfood.model.Characteristic;
import com.trustfood.trustfood.model.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.lang.reflect.ParameterizedType;

/**
 * Created by razir on 26.10.2014.
 */
public class Engine {

    static Gson gson;
    static Map<String,Characteristic> chars=new HashMap<>();
    static Map<String,Service> services=new HashMap<>();
    static  {
        gson=buildDefaultJson();
    }

    public static Gson buildDefaultJson() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        return gson;
    }

    public static void init(){
        initCharacteristics();
       initServices();
    }

    public static Characteristic findLed(int index){
        for(Map.Entry<String,Characteristic> entry:chars.entrySet()){
            Characteristic characteristic=entry.getValue();
            if(characteristic.getGroup().equals(Characteristic.GROUP_LED)
                    &&characteristic.getName().equals("Led"+index)){
                return characteristic;
            }
        }
        return null;
    }

    public static Characteristic getCharacteristic(String uuid) {
        return chars.get(uuid);
    }

    public static void initCharacteristics(){
        InputStream inputStream = null;
        try {
            inputStream = App.getmContext().getAssets().open("characteristics.json");
            Reader reader = new InputStreamReader(inputStream);
            List<Characteristic> characteristics=getObjects(reader, Characteristic.class);
            for(Characteristic characteristic:characteristics){
                chars.put(characteristic.getUuid(),characteristic);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Service getService(String uuid){
        return services.get(uuid);
    }

    public static void initServices(){
        InputStream inputStream = null;
        try {
            inputStream = App.getmContext().getAssets().open("Services.json");
            Reader reader = new InputStreamReader(inputStream);
            List<Service> servicesList=getObjects(reader, Service.class);
            for(Service service:servicesList){
                services.put(service.getUuid(),service);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static<T> List<T> getObjects(Reader reader,Class<T> cl){
        List<T> obj = gson.fromJson(reader,new ListOfSomething<T>(cl));
        return obj;
    }

    static class ListOfSomething<X> implements ParameterizedType {

        private Class<?> wrapped;

        public ListOfSomething(Class<X> wrapped) {
            this.wrapped = wrapped;
        }

        public Type[] getActualTypeArguments() {
            return new Type[] {wrapped};
        }

        public Type getRawType() {
            return List.class;
        }

        public Type getOwnerType() {
            return null;
        }
    }
}
