package com.trustfood.trustfood;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.trustfood.trustfood.utils.ByteConverter;
import com.trustfood.trustfood.utils.Hex;

import io.fabric.sdk.android.Fabric;
import java.nio.ByteBuffer;

/**
 * Created by razir on 26.10.2014.
 */
public class App extends Application {

    static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
//        Mint.initAndStartSession(this, "9bd7e860");
        mContext = getApplicationContext();
        Logger.configure();

    }


    public static Context getmContext() {
        return mContext;
    }
}
