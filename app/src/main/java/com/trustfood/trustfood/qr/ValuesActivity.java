package com.trustfood.trustfood.qr;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.trustfood.trustfood.adapter.MapAdapter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User-PC on 22.11.2014.
 */
public class ValuesActivity extends ListActivity {
    public static final String PARAM_VALUES="values";
    Map<String,String> values;
    MapAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent=getIntent();
        values=(Map<String,String>)intent.getSerializableExtra(PARAM_VALUES);
        adapter=new MapAdapter(this,values);
        setListAdapter(adapter);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        onBackPressed();
        return true;
    }
}
