package com.trustfood.trustfood.qr;



import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class QRParser {

	private static final String LOG_TAG = QRParser.class.getSimpleName();


	private QRParser() {
	}

    public static HashMap<String,String> parse(String json){
        try {
            Gson gson=new Gson();
            Type stringStringMap = new TypeToken<HashMap<String, String>>(){}.getType();
            HashMap<String,String> map = gson.fromJson(json, stringStringMap);
            return map;
        }
        catch (Exception ex){
           return null;
        }

    }

}