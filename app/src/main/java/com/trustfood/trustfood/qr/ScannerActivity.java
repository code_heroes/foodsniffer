package com.trustfood.trustfood.qr;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.Expose;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.trustfood.trustfood.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ScannerActivity extends Activity  {

	public static final String QR_MES_EXTRA = "gr_message";
	public static final String QR_ERROR = "error_message";

	private static final String LOG_TAG = ScannerActivity.class.getSimpleName();

	private Camera mCamera;

	private SurfaceView preview = null;
	private SurfaceHolder previewHolder = null;
	private boolean inPreview = false;
	private boolean cameraConfigured = false;
	private ImageView prevView;

	private boolean finished = false;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_scanner);
        getActionBar().setHomeButtonEnabled(true);
		prevView = (ImageView) findViewById(R.id.preview_image);

		preview = (SurfaceView) findViewById(R.id.cameraPreview);
		previewHolder = preview.getHolder();
		previewHolder.addCallback(surfaceCallback);
		previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        onBackPressed();
        return true;
    }

	@Override
	public void onResume() {
		super.onResume();
        try {
            mCamera = Camera.open();
            startPreview();
        }
        catch (Exception ex ){
            Toast.makeText(this,"Some problems while starting camera",Toast.LENGTH_LONG).show();
            finish();
        }

	}

	@Override
	public void onPause() {
		if (inPreview) {
			mCamera.stopPreview();
			mCamera.setPreviewCallback(null);
		}
		if (mCamera != null) {
			mCamera.lock();
			mCamera.release();
		}
		mCamera = null;
		inPreview = false;
		super.onPause();
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
			Log.e(LOG_TAG, e.getMessage());
		}
		return c;
	}

	private void releaseCamera() {
		if (mCamera != null) {
			// previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	// private Runnable doAutoFocus = new Runnable() {
	// public void run() {
	// if (inPreview)
	// //mCamera.autoFocus(autoFocusCB);
	// }
	// };

	PreviewCallback previewCb = new PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {

			if (finished || !inPreview) {
				return;
			}

			// Convert to JPG
			Size previewSize = camera.getParameters().getPreviewSize();

			YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21,
					previewSize.width, previewSize.height, null);

			Log.d(LOG_TAG,
					"w: " + yuvimage.getWidth() + " h: " + yuvimage.getHeight());
			Log.d(LOG_TAG,
					"top: " + prevView.getTop() + " left: "
							+ prevView.getLeft() + " bottom: "
							+ prevView.getBottom() + " right: "
							+ prevView.getRight());

			float imageHeight = yuvimage.getWidth();
			float imageWeight = yuvimage.getHeight();

			int top = (int) ((float) prevView.getTop() * imageHeight / (float) preview
					.getHeight());
			int left = (int) ((float) prevView.getLeft() * imageWeight / (float) preview
					.getWidth());
			int bottom = (int) ((float) prevView.getBottom() * imageHeight / (float) preview
					.getHeight());
			int right = (int) ((float) prevView.getRight() * imageWeight / (float) preview
					.getWidth());
			Log.d(LOG_TAG, "top: " + top + " left: " + left + " bottom: "
					+ bottom + " right: " + right);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			yuvimage.compressToJpeg(new Rect(top, left, bottom, right), 80,
					baos);

			byte[] jdata = baos.toByteArray();

			// Convert to Bitmap
			Bitmap bitmap = BitmapFactory.decodeByteArray(jdata, 0,
					jdata.length);
			if (bitmap == null) {
				return;
			}
			int width = bitmap.getWidth(), height = bitmap.getHeight();
			int[] pixels = new int[width * height];
			bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
			bitmap.recycle();
			bitmap = null;
			RGBLuminanceSource source = new RGBLuminanceSource(width, height,
					pixels);
			BinaryBitmap bBitmap = new BinaryBitmap(new HybridBinarizer(source));

			MultiFormatReader reader = new MultiFormatReader();
			reader.reset();
			if (reader == null || bBitmap == null) {

				return;

			}
			try {
				Result result = reader.decode(bBitmap);
				Log.e(LOG_TAG, result.getText());
				finished = true;
				finishWithResults(result.getText());
			} catch (NotFoundException e) {

				Log.e(LOG_TAG, "decode exception", e);
			}

		}
	};


	private void finishWithResults(String qrCode) {
//		Bundle conData = new Bundle();
//		conData.putString(QR_MES_EXTRA, qrCode);
        HashMap<String,String> values=QRParser.parse(qrCode);
		Intent intent = new Intent();
		intent.putExtra("data", values);
		setResult(RESULT_OK, intent);
		finish();
	}

	private void finishWithError(String mes) {
		Bundle conData = new Bundle();
		conData.putString(QR_ERROR, mes);
		Intent intent = new Intent();
		intent.putExtras(conData);
		setResult(RESULT_CANCELED, intent);
		finish();
	}

	private Size getBestPreviewSize(int width, int height,
			Camera.Parameters parameters) {
		Size result = null;

		for (Size size : parameters.getSupportedPreviewSizes()) {
			if (size.width <= width && size.height <= height) {
				if (result == null) {
					result = size;
				} else {
					int resultArea = result.width * result.height;
					int newArea = size.width * size.height;

					if (newArea > resultArea) {
						result = size;
					}
				}
			}
		}

		return (result);
	}

	private void initPreview(int width, int height) {
		if (mCamera != null && previewHolder.getSurface() != null) {
			try {
				mCamera.setPreviewDisplay(previewHolder);
			} catch (Throwable t) {
				Log.e("PreviewDemo-surfaceCallback",
						"Exception in setPreviewDisplay()", t);
				Toast.makeText(ScannerActivity.this, t.getMessage(),
						Toast.LENGTH_LONG).show();
			}

			if (!cameraConfigured) {
				Camera.Parameters parameters = mCamera.getParameters();
				Size size = getBestPreviewSize(width, height, parameters);

				if (size != null) {
					parameters.setPreviewSize(size.width, size.height);
					mCamera.setParameters(parameters);
					cameraConfigured = true;
				}
			}
		}
	}

	private void startPreview() {

		if (cameraConfigured && mCamera != null) {
			Camera.Parameters parameters = mCamera.getParameters();
			List<String> focusModes = parameters.getSupportedFocusModes();

			if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
				parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
			}
			if (focusModes
					.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
				parameters
						.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
			}
			mCamera.setParameters(parameters);

			try {
				mCamera.setPreviewDisplay(previewHolder);
			} catch (IOException e) {

				e.printStackTrace();
			}
			mCamera.setPreviewCallback(previewCb);

			mCamera.setDisplayOrientation(90);
			mCamera.startPreview();
			if (!focusModes
					.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
				mCamera.autoFocus(null);
			}
			inPreview = true;
		}
	}

	SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
		public void surfaceCreated(SurfaceHolder holder) {
			// no-op -- wait until surfaceChanged()
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			initPreview(width, height);
			startPreview();
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			// no-op
		}
	};

}