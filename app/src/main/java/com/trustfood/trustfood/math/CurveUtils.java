package com.trustfood.trustfood.math;

import android.util.Pair;

import com.trustfood.trustfood.model.Led;

import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 30.01.2015.
 */
public class CurveUtils {

    private static final int PD_START_LENGTH=600;
    private static final int PD_STEP_LENGTH=25;

    public static class Point{
        public static enum VALUE{
            X,Y
        }

        public float x,y;

        public Point(float x,float y){
            this.x=x;
            this.y=y;
        }
    }

    static enum Operation {
        Plus,Minus
    }

    public static float getMinimum(Led led){
        List<Point> points=getPoints(led);
        final WeightedObservedPoints obs = new WeightedObservedPoints();
        for(Point point:points){
            obs.add(point.x,point.y);
        }
        final PolynomialCurveFitter fitter = PolynomialCurveFitter.create(3);
        final double[] coeff = fitter.fit(obs.toList());
        PolynomialFunction function=new PolynomialFunction(coeff);
        float min=0;
        float[] result=findRoots(coeff);
        double val1=function.value(result[0]+1);
        double val2=function.value(result[0]);
        if(val1>val2){
            min=result[0];
        }
        else {
            min=result[1];
        }
//        float min=Math.min(result[0],result[1]);
//        if(min<0){
//            min=Math.max(result[0],result[1]);
//        }
        return min;
    }

    public static List<List<Point>> getMeasurement( List<Led> measurement ){
        List<List<Point>> result=new ArrayList<>();
        for(Led led: measurement){
            List<Point> points=getPoints(led);
            result.add(points);
        }
        return result;
    }

    public static List<Point> getMinSetCurve( List<List<Led>> measurements,int ledIndex){
        List<Point> result=new ArrayList<>();
        for(int i=0;i< measurements.size();++i){
            List<Led> leds=measurements.get(i);
            Led led=leds.get(ledIndex);
            float minimum=getMinimum(led);
            result.add(new Point(i,minimum));
        }
        return result;
    }

    public static float[] findRoots(double[] coeff){

        float x1=(float)getRoot(coeff,Operation.Plus);
        float x2=(float)getRoot(coeff,Operation.Minus);
        float[] result=new float[]{x1,x2};
        return result;
    }

    private static double getRoot(double[] coeff,Operation operation){
        if(coeff.length<3){
            return Float.MAX_VALUE;
        }
        else {
            double d=coeff[3],c=coeff[2],b=coeff[1];

            double t1=Math.sqrt(Math.pow(2*c,2)-4*3*d*b);
            double t2=0;

            if(operation==Operation.Plus){
                t2=-2*c+t1;
            }
            if(operation==Operation.Minus){
                t2=-2*c-t2;
            }
            double t3=t2/(2*3*d);
            return t3;
        }

    }

    private static  List<Point> getPoints(Led led){
        List<Point> points=new ArrayList<>();
        for(int i=0,waveLength=PD_START_LENGTH;i<Led.PD_COUNT;++i,waveLength+=PD_STEP_LENGTH){
            points.add(new Point((float)waveLength,led.getPD(i)));
        }
        return points;
    }
}
