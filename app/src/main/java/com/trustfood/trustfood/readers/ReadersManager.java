package com.trustfood.trustfood.readers;

/**
 * Created by razir on 07.11.2014.
 */
public class ReadersManager {

    public static final String PUMP1="Pump1";
    public static final String PUMP2="Pump2";
    public static final String LED="Led";
    public static final String MeasurementNo="Measurement No";
    public static final String TempHumi="TempHumi";
    public static final String LedDone="LED Done";

    public static ValueReader getReader(String charName){
        ValueReader valueReader=null;
        switch (charName){
            case PUMP1:
                valueReader=new PumpReader();
                break;
            case PUMP2:
                valueReader=new PumpReader();
                break;
            case LED:
                valueReader=new LedReader();
                break;
            case MeasurementNo:
                valueReader=new MeasurementNoReader();
                break;
            case TempHumi:
                valueReader=new TempHuReader();
                break;
        }

        return valueReader;
    }
}
