package com.trustfood.trustfood.readers;

import com.trustfood.trustfood.model.Characteristic;
import com.trustfood.trustfood.model.Led;
import com.trustfood.trustfood.utils.ByteConverter;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by razir on 07.11.2014.
 */
public class LedReader implements ValueReader {

    Map<String, String> values = new HashMap<>();
    Map<String, Object> realValues = new HashMap<>();

    private final Logger log = Logger.getLogger(LedReader.class.getSimpleName());

    @Override
    public boolean read(Characteristic characteristic) {
        if (characteristic == null) {
            return false;
        }
        log.debug("read");
        byte[] bytes = characteristic.getValue();
        String[] hexes = ByteConverter.arrayToHexArray(bytes, 4);
        if (hexes != null) {
            for (int i = 0; i < hexes.length; ++i) {
                try {
                    Long l = Long.parseLong(hexes[i], 16);
                    Float f = Float.intBitsToFloat(l.intValue());
//                    float valueFloat=Float.intBitsToFloat(value.intValue());
                    values.put("PD" + i, String.valueOf(f));

                    realValues.put("PD" + i, f);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            return true;
        }
        return false;
    }


    @Override
    public Map<String, String> getValues() {
        return values;
    }

    public List<Float> getPDs() {
        List<Float> result = new ArrayList<>();
        for (int i = 0; i < Led.PD_COUNT; ++i) {
            Float value = (Float) realValues.get("PD" + i);
            result.add(value);
        }
        return result;
    }

    @Override
    public Map<String, Object> getRealValues() {
        return realValues;
    }
}
