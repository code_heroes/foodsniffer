package com.trustfood.trustfood.readers;

import com.trustfood.trustfood.model.Characteristic;
import com.trustfood.trustfood.utils.ByteConverter;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by razir on 07.11.2014.
 */
public class MeasurementNoReader implements ValueReader {

    Map<String, String> values = new HashMap<>();
    private final Logger log = Logger.getLogger(MeasurementNoReader.class.getSimpleName());
    Map<String, Object> realValues = new HashMap<>();


    @Override
    public boolean read(Characteristic characteristic) {
        log.debug("read");
        byte[] bytes = characteristic.getValue();
       String[] hexes = ByteConverter.arrayToHexArray(bytes, 2);
        if (hexes != null) {

            try {
                int value = Integer.parseInt(hexes[0], 16);
                values.put("Number", String.valueOf(value));
                realValues.put("Number",value);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return true;
        }
        return false;
    }

    @Override
    public Map<String, String> getValues() {
        return values;
    }

    public Integer getNumber(){
        return (Integer)realValues.get("Number");
    }

    @Override
    public Map<String, Object> getRealValues() {
        return realValues;
    }
}
