package com.trustfood.trustfood.readers;

import com.trustfood.trustfood.model.Characteristic;
import com.trustfood.trustfood.utils.ByteConverter;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by razir on 07.11.2014.
 */
public class PumpReader implements ValueReader {

    Map<String,String> values=new HashMap<>();
    private final Logger log = Logger.getLogger(PumpReader.class.getSimpleName());


    @Override
    public boolean read(Characteristic characteristic) {
        log.debug("read");
        byte[] bytes=characteristic.getValue();
        String[]array= ByteConverter.arrayToHexArray(bytes, 1);
        if(array!=null&&array.length==2){
            String operation=array[0];
            String operName=getOperationName(operation);
            values.put("Operation",operName);

             try {
                Integer speedObj=Integer.parseInt(array[1],16);
                values.put("Speed",speedObj.toString());
            }
            catch (NumberFormatException ex){

            }

            return true;
        }
        return false;
    }

    public String getOperationName(String operation){
        Integer value=Integer.valueOf(operation,16);
        switch (value){
            case 0x6a:
                return "Forward";
            case 0x6b:
                return "Backward";
            case 0x6c:
                return "StandBy";
            case 0x6d:
                return "Break";
            default:
                return "";
        }
    }

    @Override
    public Map<String,String> getValues() {
        return values;
    }

    @Override
    public Map<String, Object> getRealValues() {
        return null;
    }
}
