package com.trustfood.trustfood.readers;

import com.trustfood.trustfood.model.Characteristic;
import com.trustfood.trustfood.utils.ByteConverter;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by razir on 07.11.2014.
 */
public class TempHuReader implements ValueReader {
    private final Logger log = Logger.getLogger(TempHuReader.class.getSimpleName());

    Map<String,String> values=new HashMap<>();


    @Override
    public boolean read(Characteristic characteristic) {
        log.debug("read");
        byte[] bytes=characteristic.getValue();
        //int[]array= ByteConverter.byte2int(bytes);
        String[] value=ByteConverter.arrayToHexArray(bytes,2);
        if(value!=null&&value.length==2){
            try {
                Integer tempObj=Integer.parseInt(value[0],16);
                double temp=175.72*tempObj.floatValue()/65536f-46.85;
                values.put("Temp",String.format("%.1f",temp));
      			Integer humObj=Integer.parseInt(value[1],16);
                double hum=125*humObj.floatValue()/65536f-6;
                values.put("Humidity",String.format("%.1f", hum));
            }
            catch (NumberFormatException ex){

            }

            return true;
        }
        return false;
    }

   

    @Override
    public Map<String,String> getValues() {
        return values;
    }

    @Override
    public Map<String, Object> getRealValues() {
        return null;
    }
}
