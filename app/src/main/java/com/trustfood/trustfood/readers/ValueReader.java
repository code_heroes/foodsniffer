package com.trustfood.trustfood.readers;

import android.util.Pair;

import com.trustfood.trustfood.model.Characteristic;

import java.util.List;
import java.util.Map;

/**
 * Created by razir on 06.11.2014.
 */
public interface ValueReader {
    public boolean read(Characteristic characteristic);
    public Map<String,String> getValues();

    public Map<String,Object> getRealValues();

}
