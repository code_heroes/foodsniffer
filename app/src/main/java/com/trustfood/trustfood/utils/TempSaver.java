package com.trustfood.trustfood.utils;

import android.os.Bundle;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.trustfood.trustfood.model.Led;


import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anton Hadutski on 10/1/14.
 */
public class TempSaver {

    static Gson gson;
    static JsonSerializer<Float> ser = new JsonSerializer<Float>() {
        @Override
        public JsonElement serialize(Float src, Type typeOfSrc, JsonSerializationContext context) {
            if (src != null) {
                return new JsonPrimitive(String.format("%.2f",src));
            } else {
                return null;
            }
        }
    };

    static List<List<Led>> measurementsSet;

    static {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Float.class,ser);
        builder.setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes fieldAttributes) {

//                Collection<Annotation> annotations = fieldAttributes.getAnnotations();
//                for (Annotation annotation : annotations) {
//                    if (annotation.annotationType().equals(Ignore.class)) {
//                        return true;
//                    }
//                }
                return false;
            }

            @Override
            public boolean shouldSkipClass(Class<?> aClass) {
                return false;
            }
        });
        builder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY);
        builder.setFieldNamingStrategy(new FieldNamingStrategy() {
            @Override
            public String translateName(Field field) {

                String str2 = field.toString();
                return str2;
//                return field.toGenericString();
            }
        });
        gson = builder.create();
    }




    public static String getJson(Object object) {
        if (object == null) {
            return null;
        }
        String json = gson.toJson(object);
        return gson.toJson(object);
    }

    public static <T> T getObject(String json, Class<T> cs) {
        if (json == null) {
            return null;
        }
        Type type = new TypeToken<T>() {
        }.getType();
        T obj = gson.fromJson(json, cs);
        return obj;
    }


    public static<T> List<T> getObjects(String json,Class<T> cl){
        if (json == null) {
            return new ArrayList<>();
        }

        List<T> obj = gson.fromJson(json,new ListOfSomething<T>(cl));
        return obj;
    }

    public static void putMeasurements(Bundle bundle,List<List<Led>> measurementsSet){
        TempSaver.measurementsSet=measurementsSet;
//        ArrayList<String>  objects=new ArrayList<>();
//        for(List<Led> obj:measurementsSet){
//            objects.add(TempSaver.getJson(obj));
//        }
//        bundle.putStringArrayList("data",objects);
    }

    public static List<List<Led>> getMeasurements(Bundle bundle){
        return TempSaver.measurementsSet;
//        List<List<Led>> measurementsSet=new ArrayList<>();
//        List<String> data=bundle.getStringArrayList("data");
//        if(data!=null) {
//            for (String list : data) {
//                measurementsSet.add(TempSaver.getObjects(list, Led.class));
//            }
//        }
//        return measurementsSet;
    }
}
