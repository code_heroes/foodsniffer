package com.trustfood.trustfood.utils;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.trustfood.trustfood.math.CurveUtils;

import java.util.List;

/**
 * Created by User-PC on 04.02.2015.
 */
public class GraphUtils {

    public static LineGraphSeries<DataPoint> convert(List<CurveUtils.Point> points){
        DataPoint[] convertedPoints=new DataPoint[points.size()];
        for(int i=0;i<points.size();++i){
            convertedPoints[i]=new DataPoint(points.get(i).x,points.get(i).y);
        }
        LineGraphSeries<DataPoint> series=new LineGraphSeries<>(convertedPoints);
        return series;
    }

    public static float[] findMaxMinY(List<CurveUtils.Point> points,CurveUtils.Point.VALUE value){
        float max=0;
        float min=Float.MAX_VALUE;
        for(CurveUtils.Point point:points){
            float val=0;
            if(value==CurveUtils.Point.VALUE.X)
            {
                val=point.x;
            }
            else {
                val=point.y;
            }
            if(val>max){
                max=val;
            }
            if(val<min){
                min=val;
            }
        }
        float[] result=new float[]{max,min};
        return result;
    }
}
