package com.trustfood.trustfood.utils;


import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Arrays;

/**
 * Created by razir on 07.11.2014.
 */
public class ByteConverter {

    public static int bytesToInt(byte[] data){
        if(data==null){
            return -1;
        }
       // int[] array=byte2int(data);
        String [] hex=arrayToString(data,data.length);
        try {
            Integer value=Integer.parseInt(hex[0],16);
            return value;
        }
        catch (NumberFormatException ex){

            return -1;
        }
    }

    public byte[][] separateArray(byte[] bytes, int parts) {
        byte[][] array = new byte[parts][];
        int length = bytes.length / parts;
        for (int i = 0; i < parts; ++i) {
            array[i] = new byte[length];
        }
        for (int i = 0; i < bytes.length; ++i) {
            int part = i / (length);
            int offset = i % (length);
            array[part][offset] = bytes[i];
        }
        return array;
    }

    public static String[] arrayToHexArray(byte[] array,int length){
        int parts = array.length / length;
        String[] result = new String[parts];
        for (int i = 0; i < parts;++i ) {
            byte[] value=getPart(array,length,i);
            result[i]=Hex.encodeHexString(value);
        }
        return result;
    }

    public static byte[] getPart(byte[] array,int length,int part){
        byte[] result=new byte[length];
        for(int i=0; i<length;++i){
            result[i]=array[part*length+i];
        }
        return result;
    }

    public static String[] arrayToString(byte[] array, int length) {
        int parts = array.length / length;
        String[][] result = new String[parts][];

        for (int i = 0; i < parts; ++i) {
            result[i] = new String[length];
        }
        for (int i = 0; i < array.length; ++i) {
            int part = i / (length);
            int offset = i % (length);

            String hex = Integer.toHexString(array[i]);
            result[part][offset] = hex;
        }
        String[] hexes = new String[result.length];
        for (int i = 0; i < result.length; ++i) {
            String hex = "";
            for (int j = 0; j < result[i].length; ++j) {
                hex += result[i][j];
            }
            hexes[i] = hex;
        }
        return hexes;
    }


    public static byte[] int2byte(int[] data){
        ByteBuffer byteBuffer = ByteBuffer.allocate(data.length * 4);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        intBuffer.put(data);
        byte[] array = byteBuffer.array();
        return array;
    }

    public static int[] byte2int(byte[] b ){
        IntBuffer intBuf =
                ByteBuffer.wrap(b)
                        .order(ByteOrder.BIG_ENDIAN)
                        .asIntBuffer();
        int[] array = new int[intBuf.remaining()];
        intBuf.get(array);
        return array;
    }



}
