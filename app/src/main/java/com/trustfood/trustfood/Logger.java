package com.trustfood.trustfood;

import android.os.Environment;

import de.mindpipe.android.logging.log4j.LogConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.helpers.DateTimeDateFormat;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.File;


/**
 * Created by eugeneshurpach on 4/28/14.
 */
public class Logger {
    private static final String FOLDER_NAME = "Trustfood";

    private static String getDay(){
        DateTimeFormatter format=ISODateTimeFormat.basicDate();
        return format.print(DateTime.now());
    }

    public static File getLogFile(){
        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + FOLDER_NAME);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return new File(folder,getDay() + ".txt");
    }

    public static void configure() {
        final LogConfigurator logConfigurator = new LogConfigurator();
        File folder = null;
      //  if (Environment.isExternalStorageEmulated()) {
        folder = new File(Environment.getExternalStorageDirectory() + File.separator + FOLDER_NAME);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        File[] files=folder.listFiles();
        if(files!=null){
            for(File file:files){
                file.delete();
            }
        }
        logConfigurator.setFileName(folder + File.separator + getDay() + ".txt");

        logConfigurator.setRootLevel(Level.DEBUG);
        logConfigurator.setUseFileAppender(true);
        logConfigurator.setImmediateFlush(true);


        // Set log level of a specific logger
        logConfigurator.setLevel("com.trustfood.trustfood", Level.ERROR);
        logConfigurator.configure();
    }

    public static void deleteOldFiles() {
//        File folder = null;
//        if (Environment.isExternalStorageEmulated()) {
//            folder = new File(Environment.getExternalStorageDirectory() + File.separator + FOLDER_NAME);
//        } else {
//            folder = new File(App.getContext().getFilesDir()+ File.separator + FOLDER_NAME);
//        }
//        if (!folder.exists()) {
//            folder.mkdirs();
//        }
//        File[] files = folder.listFiles();
//        if(files!=null) {
//            for (int i = 0; i < files.length; i++) {
//                File file = files[i];
//                long timestamp = file.lastModified();
//                DateTime dateTime = new DateTime(timestamp);
//                dateTime = dateTime.plusDays(10);
//                if (dateTime.isBeforeNow()) {
//                    file.delete();
//                }
//            }
//        }
    }

}