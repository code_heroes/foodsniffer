package com.trustfood.trustfood;

import android.bluetooth.BluetoothDevice;

import com.trustfood.trustfood.model.DeviceWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by razir on 27.10.2014.
 */
public class DeviceManager {

    static DeviceManager manager;

    List<DeviceWrapper> devices =new ArrayList<>();

    DeviceManager(){

    }

    public List<DeviceWrapper> getDevices() {
        return devices;
    }

    public static DeviceManager getInstance(){
        if(manager==null){
            manager=new DeviceManager();
        }
        return manager;
    }

    public void putDevice(String name){
        devices.add(null);
    }

    public DeviceWrapper getDevice(String address){
        for(DeviceWrapper deviceWrapper : devices){
            if(deviceWrapper.getDevice().getAddress().equals(address)){
                return deviceWrapper;
            }
        }
        return null;
    }

    public DeviceWrapper getDevice(BluetoothDevice device){
        for(DeviceWrapper deviceWrapper : devices){
            if(deviceWrapper.getDevice().getAddress().equals(device.getAddress())){
                return deviceWrapper;
            }
        }
        DeviceWrapper deviceWrapper =new DeviceWrapper(device);
        devices.add(deviceWrapper);
        return deviceWrapper;
    }

    public void disconnectAll(){
        for(DeviceWrapper wrapper:devices){
            if(wrapper.isConnected()&&wrapper.getGatt()!=null){
                wrapper.getGatt().disconnect();
            }
        }
    }
}
