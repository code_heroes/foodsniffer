package com.trustfood.trustfood.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.trustfood.trustfood.R;
import com.trustfood.trustfood.model.DeviceWrapper;

import java.util.List;

/**
 * Created by razir on 29.10.2014.
 */
public class DeviceAdapter extends ArrayAdapter<DeviceWrapper> {

    LayoutInflater inflater;

    public DeviceAdapter(Context context, int resource, List<DeviceWrapper> objects) {
        super(context, resource, objects);
        inflater=LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DeviceWrapper deviceWrapper =getItem(position);
        if(convertView==null){
            convertView=inflater.inflate(R.layout.item_qr_code,parent,false);
        }
        TextView text1=(TextView)convertView.findViewById(R.id.txt_title);
        if(deviceWrapper!=null) {

            text1.setText(R.string.device_found);
        }
        else {
            text1.setText("Hello");
        }
        return convertView;
    }
}
