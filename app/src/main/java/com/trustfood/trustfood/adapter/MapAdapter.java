package com.trustfood.trustfood.adapter;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.trustfood.trustfood.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by User-PC on 22.11.2014.
 */
public class MapAdapter extends BaseAdapter {
    public static final String separator=": ";

    Map<String,String> values=new HashMap<>();
    List<String> keys;
    Context context;
    public MapAdapter(Context context,Map<String, String> values) {
        this.values = values;
        keys=new ArrayList<>(values.keySet());
        this.context=context;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int i) {
        return keys.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if(convertView==null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_qr_code, viewGroup, false);
        }
        TextView txtTitle=(TextView)convertView.findViewById(R.id.txt_title);
        String key=(String)getItem(i);
        String value=values.get(key);
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
        final StyleSpan iss = new StyleSpan(android.graphics.Typeface.ITALIC);//Span to make text italic

        SpannableString spannableString=new SpannableString(key+separator+value);
        spannableString.setSpan(bss,0,key.length(),0);
        spannableString.setSpan(iss,key.length(),spannableString.length(),0);
        txtTitle.setText(spannableString);
       // txtTitle.setText(R.string.device_found);
        return convertView;
    }
}
