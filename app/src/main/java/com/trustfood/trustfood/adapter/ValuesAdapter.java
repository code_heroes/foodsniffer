package com.trustfood.trustfood.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.trustfood.trustfood.Engine;
import com.trustfood.trustfood.R;
import com.trustfood.trustfood.model.Characteristic;
import com.trustfood.trustfood.model.Service;
import com.trustfood.trustfood.readers.ReadersManager;
import com.trustfood.trustfood.readers.ValueReader;
import com.trustfood.trustfood.utils.ByteConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by User-PC on 22.11.2014.
 */
public class ValuesAdapter extends BaseAdapter {
    List<Characteristic> values=new ArrayList<>();
    Map<String,View> views=new HashMap<>();
    LayoutInflater inflater;
    Context context;
    OperationListener listener;

    public interface OperationListener {
        void onReadNeeds(Characteristic characteristic);
    }

    public ValuesAdapter(Context context,List<Characteristic> values,OperationListener listener) {
        this.values = values;
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.listener=listener;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int i) {
        return values.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    public void updateCharacteristic(Characteristic characteristic){
        View view=views.get(characteristic.getUuid());
        if(view!=null){
            updateCharacteristic(characteristic,view);
        }
        else {
            notifyDataSetChanged();
        }
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view=inflater.inflate(R.layout.item_characteristic,viewGroup,false);
        Characteristic characteristic=(Characteristic)getItem(i);
        views.put(characteristic.getUuid(),view);
        updateCharacteristic(characteristic,view);
        return view;

    }

    private void updateCharacteristic(Characteristic characteristic,View view){
        TextView txtCharName=(TextView)view.findViewById(R.id.txt_char_name);
        TextView txtServiceName=(TextView)view.findViewById(R.id.txt_service_name);
        TextView txtValue=(TextView)view.findViewById(R.id.txt_value);
        txtCharName.setText(characteristic.getName());
        UUID serviceUUID=characteristic.getCharacteristic().getService().getUuid();
        Service service= Engine.getService(serviceUUID.toString());
        if(service!=null){
            txtServiceName.setText(context.getString(R.string.service_name_pattern,service.getName()));
        }
        else {
            txtServiceName.setText(context.getString(R.string.service_name_pattern,serviceUUID.toString()));
        }
        String text=valueToStr(getValue(characteristic));
        if(text==null){
            txtValue.setVisibility(View.GONE);
        }
        else {
            txtValue.setText(context.getString(R.string.value_pattern,text ));
        }
    }


    private String valueToStr(Object value) {
        if (value == null) {
            return null;
        }
        if (value instanceof String) {
            return (String) value;
        }
        if (value instanceof Map) {
            StringBuilder builder = new StringBuilder();
            String pattern = "%s:%s; ";
            Map<String, String> values = (Map<String, String>) value;
            for (Map.Entry<String, String> entry : values.entrySet()) {
                String str = String.format(pattern, entry.getKey(), entry.getValue());
                builder.append(str);
            }
            return builder.toString();
        }

        return value.toString();

    }

    private Object getValue(Characteristic characteristic) {
        if (!characteristic.isSupported()) {
            return "Not supported yet";
        }
        if (characteristic.getValue() == null) {
           if(listener!=null){
               listener.onReadNeeds(characteristic);
           }
            return "In progress";
        }

        ValueReader reader = ReadersManager.getReader(characteristic.getName());
        boolean success;

        switch (characteristic.getType()) {
            case Characteristic.UTF8:

                String value = new String(characteristic.getValue());
                return value;


            case Characteristic.UINT8:
//                if (characteristic.getValueLength() == 1) {
//                    int intValue = ByteConverter.bytesToInt(characteristic.getValue());
//                    return String.valueOf(intValue);
//                } else {
                    if(reader==null){
                        int intValue = ByteConverter.bytesToInt(characteristic.getValue());
                        return String.valueOf(intValue);
                    }
                    success = reader.read(characteristic);
                    if (success) {
                        return reader.getValues();
                    } else {
                        return "Error";
                    }
               // }

            case Characteristic.UINT16:
            case Characteristic.UINT32:
            case Characteristic.INT8:
                    if(reader==null){
                        int intValue = ByteConverter.bytesToInt(characteristic.getValue());
                        return String.valueOf(intValue);
                    }
                    success = reader.read(characteristic);
                    if (success) {
                        return reader.getValues();
                    } else {
                        return "Error";
                    }
        }
        return "Error";
    }


}
