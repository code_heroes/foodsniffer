package com.trustfood.trustfood;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.trustfood.trustfood.view.MainActivity;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by User-PC on 22.11.2014.
 */
public class SplashActivity extends Activity {

    public static final int IMAGE_DELAY=3000;
    public static final int ANIMATION_TIME=100;
    int[] images=new int[]{R.drawable.seventh_framework,
            R.drawable.splash,
           };
    Timer timer;
    ImageView imageView;
    int current=-1;
    Handler handler=new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        imageView=(ImageView)findViewById(R.id.image);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(timer!=null) {
                    timer.cancel();
                }
                scheduleTimer();
            }
        });
        scheduleTimer();
    }

    public void scheduleTimer(){
        timer=new Timer();
        timer.scheduleAtFixedRate(new ImageTimerTask(),0,IMAGE_DELAY);
    }

    public void startMainActivity(){
        if(timer!=null){
            timer.cancel();
        }
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    public class ImageTimerTask extends TimerTask {

        @Override
        public void run() {
            current++;
            if(current<images.length) {

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        changeImage(images[current],imageView);
                    }
                });

            }
            else {
                startMainActivity();
            }

        }
    }

    public void changeImage(final int newImage, final ImageView imageView){
        ObjectAnimator animatorIn=ObjectAnimator.ofFloat(imageView,"alpha",1f,0);
        if(imageView.getDrawable()!=null) {
            animatorIn.setDuration(ANIMATION_TIME);
        }
        animatorIn.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                imageView.setImageResource(newImage);
                ObjectAnimator animatorOut=ObjectAnimator.ofFloat(imageView,"alpha",0,1f);
                animator.setDuration(ANIMATION_TIME);
                animatorOut.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animatorIn.start();
    }
}
