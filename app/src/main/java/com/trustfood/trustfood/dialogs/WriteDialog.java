package com.trustfood.trustfood.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.trustfood.trustfood.BluetoothLeService;
import com.trustfood.trustfood.R;
import com.trustfood.trustfood.model.Characteristic;
import com.trustfood.trustfood.model.DeviceWrapper;
import com.trustfood.trustfood.writers.ValueWriter;
import com.trustfood.trustfood.writers.ValueWriterResolver;
import com.trustfood.trustfood.writers.WritersManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by razir on 10.11.2014.
 */
public class WriteDialog extends DialogFragment {

    Characteristic characteristic;
    BluetoothLeService service;
    DeviceWrapper deviceWrapper;

    public static WriteDialog getInstance(Characteristic characteristic,BluetoothLeService service,
                                          DeviceWrapper deviceWrapper){
        WriteDialog dialog=new WriteDialog();
        dialog.service=service;
        dialog.characteristic=characteristic;
        dialog.deviceWrapper=deviceWrapper;
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_write_item, null);
        ValueWriter writer = WritersManager.getWriter(characteristic.getName());
        TextView txtTitle=(TextView)view.findViewById(R.id.txt_title);
        txtTitle.setText(characteristic.getName());
        final List<ValueWriterResolver> values = writer.getValues();
        final Spinner spinnerOptions = (Spinner) view.findViewById(R.id.spi_options);
        final EditText edValue = (EditText) view.findViewById(R.id.ed_value);

        final Spinner spinner = (Spinner) view.findViewById(R.id.spi_value);
        ArrayAdapter<ValueWriterResolver> adapter = new ArrayAdapter<> (getActivity(),
                 android.R.layout.simple_spinner_item,values);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ValueWriterResolver resolver=values.get(i);
                if(resolver.isAnyValue()){
                    spinnerOptions.setVisibility(View.GONE);
                    edValue.setVisibility(View.VISIBLE);
                }
                else if( resolver.hasOptions()){
                    spinnerOptions.setVisibility(View.VISIBLE);
                    edValue.setVisibility(View.GONE);
                    Map<String,String> values=resolver.getOptions();
                    List<String> list=new ArrayList<>();
                    list.addAll(values.keySet());
                    ArrayAdapter<String> adapter = new ArrayAdapter<> (getActivity(),
                            android.R.layout.simple_spinner_item,list);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerOptions.setAdapter(adapter);
                }
                else {
                    spinnerOptions.setVisibility(View.GONE);
                    edValue.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        builder.setView(view);
        spinner.setSelection(0);
        builder.setPositiveButton(R.string.dialog_write,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ValueWriterResolver resolver=(ValueWriterResolver)spinner.getSelectedItem();
                byte[] value=null;
                if(resolver.isAnyValue()){
                    try {
                        Integer integer=Integer.valueOf(edValue.getText().toString());
                        value=resolver.getValue(integer);
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
                else if(resolver.hasOptions()){
                    String key=(String)spinnerOptions.getSelectedItem();
                    value=resolver.getValue(key);
                }
                else {
                    value=resolver.getValue();
                }
                characteristic.setValue(value);
                service.writeCharacteristic(deviceWrapper,characteristic);
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.dialog_cancel,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        return builder.create();
    }


}
