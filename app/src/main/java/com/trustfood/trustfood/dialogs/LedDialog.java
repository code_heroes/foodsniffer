package com.trustfood.trustfood.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.trustfood.trustfood.R;
import com.trustfood.trustfood.model.Characteristic;
import com.trustfood.trustfood.readers.ReadersManager;
import com.trustfood.trustfood.readers.ValueReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by razir on 10.11.2014.
 */
public class LedDialog extends DialogFragment {


    Characteristic characteristic;

    public static LedDialog getInstance(Characteristic characteristic) {
        LedDialog dialog = new LedDialog();
        dialog.characteristic = characteristic;
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ValueReader reader=ReadersManager.getReader(ReadersManager.LED);
//        try {
            boolean result = reader.read(characteristic);
//        }
//        catch ()
        if(result){
            List<String> values=new ArrayList<>();
            for(Map.Entry<String,String> entry:reader.getValues().entrySet()){
                values.add(entry.getKey()+": "+entry.getValue());
            }
            String[] array = values.toArray(new String[values.size()]);
            builder.setItems(array,null);
        }

        builder.setNegativeButton(R.string.dialog_cancel,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        return builder.create();
    }
}
