package com.trustfood.trustfood.model;

import android.bluetooth.BluetoothGattCharacteristic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by razir on 26.10.2014.
 */
public class Characteristic {
    public static final String TYPE_READ="r";
    public static final String TYPE_READ_WRITE="rw";

    public static final String UINT8="uint8";
    public static final String UTF8="utf8";
    public static final String UINT16="uint16";
    public static final String UINT32="uint32";
    public static final String INT8="int8";

    public static final String GROUP_LED="led";
    public static final String GROUP_INFO="info";
    public static final String GROUP_WRITE="write";

    BluetoothGattCharacteristic characteristic;

    @Expose
    @SerializedName("group")
    String group;

    @Expose
    @SerializedName("name")
    String name;
    @Expose
    @SerializedName("uuid")
    String uuid;
    @Expose
    @SerializedName("measurable")
    boolean measurable=false;

    byte[] value;
    @Expose
    @SerializedName("valueLength")
    int valueLength;

    @Expose
    @SerializedName("type")
    String type;

    @Expose
    @SerializedName("perm")
    String perm=TYPE_READ;

    @Expose
    @SerializedName("supported")
    boolean supported=true;

    @Expose
    @SerializedName("order")
    int order;

    public BluetoothGattCharacteristic getCharacteristic() {
        return characteristic;
    }

    public int getOrder() {
        return order;
    }

    public boolean isSupported() {
        return supported;
    }

    public void setCharacteristic(BluetoothGattCharacteristic characteristic) {
        this.characteristic = characteristic;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isMeasurable() {
        return measurable;
    }

    public void setMeasurable(boolean measurable) {
        this.measurable = measurable;
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value=value;
        if(characteristic!=null) {
            characteristic.setValue(value);
        }
    }

    public int getValueLength() {
        return valueLength;
    }

    public void setValueLength(int valueLength) {
        this.valueLength = valueLength;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPerm() {
        return perm;
    }

    public void setPerm(String perm) {
        this.perm = perm;
    }
}
