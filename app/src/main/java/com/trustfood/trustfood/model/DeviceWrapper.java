package com.trustfood.trustfood.model;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Created by razir on 27.10.2014.
 */
public class DeviceWrapper {

    public DeviceWrapper(BluetoothDevice device) {
        this.device = device;
    }

    Map<String,Characteristic> characteristics=new HashMap<>();
    BluetoothGatt gatt;
    BluetoothDevice device;
    boolean connected;
    int rssi;

    public int getRssi() {
        return rssi;
    }

    public Collection<Characteristic> getCharacteristics(){
        Collection<Characteristic> values=characteristics.values();
        List<Characteristic> result=new ArrayList<>(values);
        Collections.sort(result,new Comparator<Characteristic>() {
            @Override
            public int compare(Characteristic o, Characteristic o2) {
                if(o.getOrder()>o2.getOrder()){
                    return 1;
                }
                else {
                    return -1;
                }
            }
        });
        return result;
    }

    public Characteristic getCharByName(String name){
        for(Map.Entry<String,Characteristic> entry:characteristics.entrySet()){
            Characteristic characteristic=entry.getValue();
            if(characteristic.getName().equals(name)){
                return characteristic;
            }
        }
        return null;
    }

    public Characteristic getCharacteristic(String uuid){
        return characteristics.get(uuid);
    }

    public void putCharacteristic(String uuid,Characteristic cha){
        characteristics.put(uuid,cha);
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public String getAddress(){
        return device.getAddress();
    }

    public BluetoothGatt getGatt() {
        return gatt;
    }

    public void setGatt(BluetoothGatt gatt) {
        this.gatt = gatt;
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public void setDevice(BluetoothDevice device) {
        this.device = device;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}
