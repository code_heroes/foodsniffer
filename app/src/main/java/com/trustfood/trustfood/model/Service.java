package com.trustfood.trustfood.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by razir on 26.10.2014.
 */
public class Service {

    @Expose
    @SerializedName("uuid")
    String uuid;
    @Expose
    @SerializedName("name")
    String name;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
