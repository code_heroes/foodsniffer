package com.trustfood.trustfood.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User-PC on 28.01.2015.
 */
public class Led {

    public static final int LED_COUNT=10;
    public static final int PD_COUNT=10;

    @SerializedName("pd")
    List<Float> pds=new ArrayList<>();

    public Led() {
    }

    public Led(List<Float> pds) {
        this.pds = pds;
    }

    public Float getPD(int i){
        return pds.get(i);
    }

    public List<Float> getPds() {
        return pds;
    }
}
