package com.trustfood.trustfood;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.trustfood.trustfood.model.Characteristic;
import com.trustfood.trustfood.model.DeviceWrapper;
import com.trustfood.trustfood.model.Led;
import com.trustfood.trustfood.readers.LedReader;
import com.trustfood.trustfood.readers.ReadersManager;
import com.trustfood.trustfood.utils.ByteConverter;
import com.trustfood.trustfood.writers.ValueWriter;
import com.trustfood.trustfood.writers.ValueWriterResolver;
import com.trustfood.trustfood.writers.WritersManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

// BluetoothLeService - manages connections and data communication with given Bluetooth LE devices.
public class BluetoothLeService extends Service {
    private final static String TAG = BluetoothLeService.class.getSimpleName();
    private final Logger log = Logger.getLogger(BluetoothLeService.class.getSimpleName());
    boolean readRightNow;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothManager mBluetoothManager;
    private boolean mScanning;
    private Handler mHandler;
    MeasurementsReader reader;
    private final IBinder mBinder = new LocalBinder();
    Queue<BluetoothGattCharacteristic> queue = new LinkedList<>();
    public static final String ACTION_START_SCAN = "com.trustfood.trustfood.ACTION_START_SCAN";
    public static final String ACTION_STOP_SCAN = "com.trustfood.trustfood.ACTION_STOP_SCAN";
    public static final String ACTION_DEVICE_DISCOVERED = "com.trustfood.trustfood.ACTION_DEVICE_DISCOVERED";
    public static final String ACTION_GATT_CONNECTED = "com.trustfood.trustfood.ACTION_GATT_CONNECTED";
    public static final String ACTION_GATT_DISCONNECTED = "com.trustfood.trustfood.ACTION_GATT_DISCONNECTED";
    public static final String ACTION_GATT_CONNECTION_STATE_ERROR = "com.trustfood.trustfood.ACTION_GATT_CONNECTION_STATE_ERROR";
    public static final String ACTION_GATT_SERVICES_DISCOVERED = "com.trustfood.trustfood.ACTION_GATT_SERVICES_DISCOVERED";
    public static final String ACTION_DATA_AVAILABLE = "com.trustfood.trustfood.ACTION_DATA_AVAILABLE";
    public static final String ACTION_DATA_WRITE = "com.trustfood.trustfood.ACTION_DATA_WRITE";
    public static final String ACTION_READ_REMOTE_RSSI = "com.trustfood.trustfood.ACTION_READ_REMOTE_RSSI";
    public static final String ACTION_DESCRIPTOR_WRITE = "com.trustfood.trustfood.ACTION_DESCRIPTOR_WRITE";

    public static final String FoodSnifferPrefs = "FoodSnifferBLE";
    public static final String ReadInterval="Read Interval"; // SharedPreference key for read interval (ms)
    public static final String SendAvgSettings="SendAvgSettings"; // SharedPreference key for sending the average settings to the sensor

    /* settings for average which are read using getSettings() from the sharedpreferences in onBind,
       and possibly updated at another opportunity that they can be read safely from the right thread */
    private static int currentValue=0;
    private static int delayBefore=150;
    private static int delayAfter=150;
    private static int measuresAvg=10;
    private static int measuresInterval=20;
    private static int readInterval=50; // read interval in milliseconds to be used from the Android side
    private static boolean sendAvgSettings=true; // enable/disable sending the avg settings to the sensor


    public static final String SCAN_PERIOD = "scanPeriod";
    public static final String DISCOVERED_DEVICE = "discoveredDevice";
    public static final String DEVICE = "device";
    public static final String DEVICE_ADDRESS = "deviceAddress";
    public static final String RSSI = "rssi";
    public static final String UUID_CHARACTERISTIC = "uuidCharacteristic";
    public static final String UUID_DESCRIPTOR = "uuidDescriptor";
    public static final String GATT_STATUS = "gattStatus";
    public static final String SCAN_RECORD = "scanRecord";
    static List<String> blackList = new ArrayList<>();
    Timer readTimer;

    static {
//        blackList.add("89206ec3-04a2-45c1-b3d2-d47c45547ac0");
//        blackList.add("da0f141b-d042-4f92-a825-b4c3f1471d96");
//        blackList.add("57273808-8c7e-4306-bfb1-2b625dee3f47");
//        blackList.add("067ccc35-71aa-4f42-8ec2-01e64043ec94");
//        blackList.add("4f3a1f34-d02c-4f2a-b036-094e0a41aa06");
//        blackList.add("6966ab66-0c66-4e00-ae4e-17a4d23ebaae");
//        blackList.add("4f0f0626-08ed-425e-a9d3-0d50b7806788");
//        blackList.add("7cac2f98-b984-4b29-92e8-eae3c7a9c311");
    }

    public void runReadTimer(DeviceWrapper wrapper) {

        if (readTimer == null) {
            readTimer = new Timer(true);
            readTimer.scheduleAtFixedRate(new ReadTimer(wrapper), 0, readInterval);
        }

    }

    private class ReadTimer extends TimerTask {
        DeviceWrapper wrapper;

        private ReadTimer(DeviceWrapper wrapper) {
            this.wrapper = wrapper;
        }

        @Override
        public void run() {
            if (!queue.isEmpty() && !readRightNow) {
                readRightNow = true;
                BluetoothGattCharacteristic characteristic = queue.poll();
                wrapper.getGatt().readCharacteristic(characteristic);

            }
        }
    }

    public void simulate() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                DeviceManager.getInstance().putDevice("sds");
                Intent broadcastIntent = new Intent(ACTION_DEVICE_DISCOVERED);
                sendBroadcast(broadcastIntent);
            }
        }, 5000);
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        // Called when new BLE device is discovered
        // Broadcast intent is sent with following extras: device , rssi,
        // additional advertise data
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

            DeviceWrapper deviceWrapper = DeviceManager.getInstance().getDevice(device);
            deviceWrapper.setRssi(rssi);

            Intent broadcastIntent = new Intent(ACTION_DEVICE_DISCOVERED);

            sendBroadcast(broadcastIntent);
            log.debug("onLeScan - device: " + device.getAddress());
            Log.i("BLE service", "onLeScan - device: " + device.getAddress() + " - rssi: " + rssi);
        }
    };


    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            log.debug("Connection state change device-" + gatt.getDevice().getAddress() + ", state="
                    + newState + ", status-" + status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    DeviceWrapper device = DeviceManager.getInstance().getDevice(gatt.getDevice());
                    device.setGatt(gatt);
                    device.setConnected(true);

                    // } else {
                    Intent updateIntent = new Intent(ACTION_GATT_CONNECTED);
                    updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                    sendBroadcast(updateIntent);
                    //}
//                    gatt.discoverServices();
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    if (readTimer != null) {
                        readTimer.cancel();
                    }

                    DeviceWrapper device = DeviceManager.getInstance().getDevice(gatt.getDevice());
                    device.setConnected(false);
                    Intent updateIntent = new Intent(ACTION_GATT_DISCONNECTED);
                    updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                    sendBroadcast(updateIntent);
                }
            } else {
                DeviceWrapper device = DeviceManager.getInstance().getDevice(gatt.getDevice());
                device.setConnected(false);
                Intent updateIntent = new Intent(ACTION_GATT_CONNECTION_STATE_ERROR);
                updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                sendBroadcast(updateIntent);
            }
            Log.i("BLE service", "onConnectionStateChange - status: " + status + " - new state: " + newState);

        }

        // Called when services are discovered on remote device
        // If success broadcast with device address extra is sent
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                DeviceWrapper device = DeviceManager.getInstance().getDevice(gatt.getDevice());
                device.setGatt(gatt);
                for (BluetoothGattService service : gatt.getServices()) {
                    for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                        log.debug("Characteristic discovered: " + characteristic.getUuid());
                        Characteristic cha = Engine.getCharacteristic(characteristic.getUuid().toString());
                        if (cha != null) {
                            cha.setCharacteristic(characteristic);
                            device.putCharacteristic(characteristic.getUuid().toString(), cha);
                        }
                    }
                }
                Intent updateIntent = new Intent(ACTION_GATT_SERVICES_DISCOVERED);
                updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                sendBroadcast(updateIntent);

            }

            log.debug("onServicesDiscovered - status: " + status);
            Log.i("BLE service", "onServicesDiscovered - status: " + status);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            DeviceWrapper wrapper = DeviceManager.getInstance().getDevice(gatt.getDevice());
            Characteristic cha = Engine.getCharacteristic(characteristic.getUuid().toString());
            if (cha == null) {
                return;
            }
            cha.setCharacteristic(characteristic);

            cha.setValue(characteristic.getValue());
            wrapper.putCharacteristic(characteristic.getUuid().toString(), cha);
            Intent updateIntent = new Intent(ACTION_DATA_AVAILABLE);
            updateIntent.putExtra(UUID_CHARACTERISTIC, characteristic.getUuid().toString());
            updateIntent.putExtra(GATT_STATUS, status);
            sendBroadcast(updateIntent);
            log.debug("onCharacteristicRead - status: " + status + "  - UUID: " + characteristic.getUuid() +
                    "service uuid=" + characteristic.getService().getUuid().toString());
            Log.i("BLE service", "onCharacteristicRead - status: " + status + "  - UUID: " + characteristic.getUuid());
            //if (queue.isEmpty()) {
            readRightNow = false;
//            } else {
//                BluetoothGattCharacteristic next = queue.poll();
//                gatt.readCharacteristic(next);
//            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Characteristic cha = Engine.getCharacteristic(characteristic.getUuid().toString());
                if (cha == null) {
                    return;
                }
                cha.setCharacteristic(characteristic);
                cha.setValue(characteristic.getValue());
                Intent updateIntent = new Intent(ACTION_DATA_WRITE);
                updateIntent.putExtra(UUID_CHARACTERISTIC, characteristic.getUuid().toString());
                updateIntent.putExtra(GATT_STATUS, status);
                sendBroadcast(updateIntent);
            }
            log.debug("onCharacteristicWrite - status: " + status + "  - UUID: " + characteristic.getUuid());
            Log.i("BLE service", "onCharacteristicWrite - status: " + status + "  - UUID: " + characteristic.getUuid());
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                DeviceWrapper device = DeviceManager.getInstance().getDevice(gatt.getDevice());
                device.setRssi(rssi);
                Intent updateIntent = new Intent(ACTION_READ_REMOTE_RSSI);
                updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                sendBroadcast(updateIntent);
            }
            Log.i("BLE service", "onReadRemoteRssi - status: " + status);
        }


        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Intent updateIntent = new Intent(ACTION_DESCRIPTOR_WRITE);
            updateIntent.putExtra(GATT_STATUS, status);
            updateIntent.putExtra(UUID_DESCRIPTOR, descriptor.getUuid());
            sendBroadcast(updateIntent);
            Log.i("BLE service", "onDescriptorWrite - status: " + status + "  - UUID: " + descriptor.getUuid());
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Intent updateIntent = new Intent(ACTION_DATA_AVAILABLE);

            DeviceWrapper wrapper = DeviceManager.getInstance().getDevice(gatt.getDevice());
            Characteristic cha = Engine.getCharacteristic(characteristic.getUuid().toString());
            cha.setCharacteristic(characteristic);
            cha.setValue(characteristic.getValue());
            wrapper.putCharacteristic(characteristic.getUuid().toString(), cha);
            updateIntent.putExtra(DEVICE_ADDRESS, wrapper.getAddress());
            updateIntent.putExtra(UUID_CHARACTERISTIC, characteristic.getUuid().toString());
            sendBroadcast(updateIntent);
            log.debug("onCharacteristicChanged - status: " + "  - UUID: " + characteristic.getUuid());
            Log.i("BLE service", "onCharacteristicChanged - status: " + "  - UUID: " + characteristic.getUuid());
        }
    };


    public void startScanning(final int scanPeriod) {
        //simulate();
        mHandler.postDelayed(new Runnable() {
            // Called after scanPeriod milliseconds elapsed
            // It stops scanning and sends broadcast
            @Override
            public void run() {
                mScanning = false;
                mBluetoothAdapter.stopLeScan(mLeScanCallback);

                Intent broadcastIntent = new Intent(ACTION_STOP_SCAN);
                sendBroadcast(broadcastIntent);

            }
        }, scanPeriod);
        mScanning = true;
        mBluetoothAdapter.startLeScan(mLeScanCallback);

    }

    public void stopScanning() {
        mScanning = false;
        mBluetoothAdapter.stopLeScan(mLeScanCallback);

        Intent broadcastIntent = new Intent(ACTION_STOP_SCAN);
        sendBroadcast(broadcastIntent);
    }

    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    public void getSettings(SharedPreferences prefs) {
        currentValue=prefs.getInt(WritersManager.CurrentValue,0); // FIXME default not available in spec
        delayBefore=prefs.getInt(WritersManager.DelayBefore,150);
        delayAfter=prefs.getInt(WritersManager.DelayAfter,150);
        measuresAvg=prefs.getInt(WritersManager.MeasurementAvg,10);
        measuresInterval=prefs.getInt(WritersManager.MeasurementInterval,20);
        readInterval=prefs.getInt(ReadInterval,50);
        sendAvgSettings=prefs.getBoolean(SendAvgSettings,true);
        log.debug("Settings retrieved: currentValue="+currentValue+", delayBefore="+delayBefore+", delayAfter="+delayAfter+", " +
                "avg="+measuresAvg+", measInterval="+measuresInterval+", readInterval"+readInterval+", sendAvgSettings="+sendAvgSettings);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    public boolean initialize() {
        mHandler = new Handler();
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }
        // read the settings upon succesful Bluetooth setup
        SharedPreferences prefs = getSharedPreferences(FoodSnifferPrefs, Context.MODE_PRIVATE);
        getSettings(prefs);

        return true;
    }

    // -----------------------------------------------------------------------
    // Following methods are available from app and they operate tasks related
    // to Bluetooth Low Energy technology
    // -----------------------------------------------------------------------

    // Connects to given device


    public boolean connect(DeviceWrapper device) {

        if (mBluetoothAdapter == null || device == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        BluetoothGatt bluetoothGatt = device.getDevice().connectGatt(this, false, mGattCallback);
        device.setGatt(bluetoothGatt);

        return true;
    }

    // Disconnects from given device
    public void disconnect(DeviceWrapper device) {
        device.getGatt().disconnect();
    }

    // Reads value for given characteristic
    public void readCharacteristic(DeviceWrapper device, BluetoothGattCharacteristic charact) {
        if (blackList.contains(charact.getUuid().toString())) {
            return;
        }

//        if (queue.isEmpty() && !readRightNow) {
//            readRightNow = true;
//
//            device.getGatt().readCharacteristic(charact);
//        } else {
        queue.add(charact);
//        }
        runReadTimer(device);
    }

    public void readAllCharacteristics(DeviceWrapper device) {
        List<BluetoothGattService> services = device.getGatt().getServices();
        log.debug("services-" + services.toString());
        for (BluetoothGattService service : services) {
            List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
            log.debug("characteristics-" + characteristics.toString());
            for (BluetoothGattCharacteristic characteristic : characteristics) {
                readCharacteristic(device, characteristic);
            }
        }
    }

    // Writes value for given characteristic
    public boolean writeCharacteristic(DeviceWrapper device, Characteristic charact) {
        if (device != null && device.getGatt() != null && charact != null) {
            return device.getGatt().writeCharacteristic(charact.getCharacteristic());
        }
        return false;
    }

    // Enables or disables characteristic notification
    public boolean setCharacteristicNotification(DeviceWrapper device, BluetoothGattCharacteristic charact, boolean enabled) {
        return device.getGatt().setCharacteristicNotification(charact, enabled);
    }

    // Writes value for given descriptor
    public boolean writeDescriptor(DeviceWrapper device, BluetoothGattDescriptor descriptor) {
        return device.getGatt().writeDescriptor(descriptor);
    }

    // Reads rssi for given device
    public boolean readRemoteRssi(DeviceWrapper device) {
        return device.getGatt().readRemoteRssi();
    }

    // Close all established connections
    public void close() {
        for (DeviceWrapper device : DeviceManager.getInstance().getDevices()) {
            if (device.getGatt() != null) {
                device.getGatt().close();
                device.setGatt(null);
            }
        }
    }

    public List<List<Led>> stopMeasure() {
        if (reader != null) {
            List<List<Led>> result = reader.stop();
            reader = null;
            return result;
        }
        return null;
    }

    public boolean isMeasuringRunning() {
        return reader != null;
    }

    // Checks if service is currently scanning for new BLE devices
    public boolean isScanning() {
        return mScanning;
    }

    public MeasurementsReader startReadAllLeds(final DeviceWrapper deviceWrapper, Context context,
                                               MeasurementListener listener) {
        reader = new MeasurementsReader(deviceWrapper, context, listener);
        reader.execute();
        return reader;
    }

    public static interface MeasurementListener {
        void onFinished(List<List<Led>> measurementsSet);

        void onProgressChanged(int current, int max);
    }

    public class MeasurementsReader extends AsyncTask<Void, Integer, Void> {
        public static final int READS_COUNT = 400;
        DeviceWrapper deviceWrapper;
        ValueWriterResolver measurementNoRes, loadNextRes, currentValueRes, delayAfterRes, delayBeforeRes, measurementAvgRes, measurementIntervalRes;
        List<Characteristic> ledChars = new ArrayList<>();
        Characteristic measurementNoChar, commandChar,ledDoneChar, currentValueChar, delayAfterChar, delayBeforeChar, measurementAvgChar, measurementIntervalChar;
        BroadcastReceiver receiver;
        List<Led> leds = new ArrayList<>();
        List<List<Led>> measurementsSet = new ArrayList<>();
        Context context;
        Handler handler = new Handler(Looper.getMainLooper());
        MeasurementListener listener;

        public MeasurementsReader(DeviceWrapper deviceWrapper, Context context,
                                  MeasurementListener listener) {
            this.deviceWrapper = deviceWrapper;
            this.context = context;
            this.listener = listener;
        }

        public List<List<Led>> stop() {
            context.unregisterReceiver(receiver);
            cancel(true);
            return measurementsSet;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // read settings on main thread before starting background task
            SharedPreferences prefs = getSharedPreferences(FoodSnifferPrefs, Context.MODE_PRIVATE);
            getSettings(prefs);

            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String uuid = null;
                    Characteristic characteristic = null;
                    final String action = intent.getAction();
                    log.debug("action-" + action);
                    switch (action) {
                        case BluetoothLeService.ACTION_GATT_DISCONNECTED:
                            break;
                        case BluetoothLeService.ACTION_DATA_AVAILABLE:
                            uuid = intent.getStringExtra(BluetoothLeService.UUID_CHARACTERISTIC);
                            characteristic = deviceWrapper.getCharacteristic(uuid);
                            if (characteristic.getGroup().equals("led")) {
                                readLed(characteristic);
                                resolveLeds();
                            }
                            if(characteristic.getName().equals(ledDoneChar.getName())){
                                if (!isLedsFinished(characteristic)) {
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            log.debug("readLedDone handler");
                                            readLedDone(ledDoneChar);
                                        }
                                    }, 1000);
                                } else {
                                    readAllLeds();
                                }
                            }
                            if (characteristic.getName().equals(commandChar.getName())) {
                                if (!isCommandFinished(characteristic)) {
                                    writeLoadNext(characteristic);
                                } else {
                                    readLedDone(ledDoneChar);
                                }
                            }
                            break;
                        case BluetoothLeService.ACTION_DATA_WRITE:
                            uuid = intent.getStringExtra(BluetoothLeService.UUID_CHARACTERISTIC);
                            characteristic = deviceWrapper.getCharacteristic(uuid);
                            if (characteristic.getName().equals(measurementNoChar.getName())) {
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        writeLoadNext(commandChar);
                                    }
                                }, 500);

                            }
                            if (characteristic.getName().equals(commandChar.getName())) {
                                if (!isCommandFinished(characteristic)) {
                                    final Characteristic ch = characteristic;
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            readCommand(ch);
                                        }
                                    }, 2000);

                                } else {
                                    readLedDone(ledDoneChar);
                                }
                            }
                            break;
                    }
                }
            };
            context.registerReceiver(receiver, getGattUpdateIntentFilter());

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if (listener != null) {
                listener.onProgressChanged(values[0], values[1]);
            }
        }

        private IntentFilter getGattUpdateIntentFilter() {

            IntentFilter bleIntentFilter = new IntentFilter();
            bleIntentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
            bleIntentFilter.addAction(BluetoothLeService.ACTION_DATA_WRITE);
            bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
            //bleIntentFilter.addAction(BluetoothLeService.ACTION_READ_REMOTE_RSSI);
            return bleIntentFilter;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            log.debug("device writing in background started");
            measurementNoChar = deviceWrapper.getCharByName(WritersManager.MeasurementNo);
            commandChar = deviceWrapper.getCharByName(WritersManager.COMMAND);
            ledDoneChar = deviceWrapper.getCharByName(ReadersManager.LedDone);
            measurementAvgChar = deviceWrapper.getCharByName(WritersManager.MeasurementAvg);
            measurementIntervalChar = deviceWrapper.getCharByName(WritersManager.MeasurementInterval);
            delayAfterChar = deviceWrapper.getCharByName(WritersManager.DelayAfter);
            delayBeforeChar = deviceWrapper.getCharByName(WritersManager.DelayBefore);
            for (int i = 0; i < Led.LED_COUNT; ++i) {
                ledChars.add(deviceWrapper.getCharByName("Led" + i));
            }
            if (sendAvgSettings) {
                log.debug("device writing avg settings");
                writeMeasurmentInterval(measurementIntervalChar, measuresInterval);
                writeDelayBefore(delayBeforeChar, delayBefore);
                writeDelayAfter(delayAfterChar, delayAfter);
                writeMeasurementAvg(measurementAvgChar, measuresAvg);
                writeCurrentValue(currentValueChar, currentValue);
            }
            writeNumber(measurementNoChar, 0);
            log.debug("device writing in background finished");
            return null;
        }

        private void resolveLeds() {
            if (leds.size() == Led.LED_COUNT) {
                measurementsSet.add(leds);
                if (measurementsSet.size() < READS_COUNT) {
                    leds = new ArrayList<>();
                    publishProgress(measurementsSet.size(), READS_COUNT);
                    writeLoadNext(commandChar);
                } else {
                    if (listener != null) {
                        reader = null;
                        context.unregisterReceiver(receiver);
                        listener.onFinished(measurementsSet);
                    }
                }
            }
        }

        private void writeNumber(Characteristic characteristic, int i) {
            if (characteristic==null) {
                log.warn("writeNumber char=null");
                return;
            }
            if (measurementNoRes == null) {
                ValueWriter writer = WritersManager.getWriter(characteristic.getName());
                measurementNoRes = getResolver(WritersManager.MeasurementNo, writer.getValues());
            }
            if (measurementNoRes == null) {
                log.warn("writeNumber resolver not available");
                return;
            }
            byte[] value = measurementNoRes.getValue(i);
            characteristic.setValue(value);
            writeCharacteristic(deviceWrapper, characteristic);
	    }

        private void writeLoadNext(Characteristic characteristic) {
            if (characteristic==null) {
                log.warn("writeLoadNext char=null");
                return;
            }
            if (loadNextRes == null) {
                ValueWriter writer = WritersManager.getWriter(characteristic.getName());
                loadNextRes = getResolver("StartMeasure", writer.getValues());
            }
            if (loadNextRes == null) {
                log.warn("writeLoadNext resolver not available");
                return;
            }
            byte[] value = loadNextRes.getValue();
            characteristic.setValue(value);
            writeCharacteristic(deviceWrapper, characteristic);
        }

        private void writeCurrentValue(Characteristic characteristic, int i) {
            if (characteristic==null) {
                log.warn("writeCurrentValue char=null");
                return;
            }
            if (currentValueRes==null) {
                ValueWriter writer = WritersManager.getWriter(characteristic.getName());
                currentValueRes=getResolver(WritersManager.CurrentValue,writer.getValues());
            }
            if (currentValueRes==null) {
                log.warn("writeCurrentValue revolver not available");
                return;
            }
            log.debug("writing current value=" + i);
            characteristic.setValue(currentValueRes.getValue(i));
            writeCharacteristic(deviceWrapper, characteristic);
        }

        private void writeDelayAfter(Characteristic characteristic, int i) {
            if (characteristic==null) {
                log.warn("writeDelayAfter char=null");
                return;
            }
            if (delayAfterRes==null) {
                ValueWriter writer = WritersManager.getWriter(characteristic.getName());
                delayAfterRes = getResolver(WritersManager.DelayAfter, writer.getValues());
            }
            if (delayAfterRes==null) {
                log.warn("writeDelayAfter resolver not available");
                return;
            }
            log.debug("writing delayAfter=" + i);
            characteristic.setValue(delayAfterRes.getValue(i));
            writeCharacteristic(deviceWrapper, characteristic);
        }

        private void writeDelayBefore(Characteristic characteristic, int i) {
            if (characteristic==null) {
                log.warn("writeDelayBefore char=null");
                return;
            }
            if (delayBeforeRes==null) {
                ValueWriter writer = WritersManager.getWriter(characteristic.getName());
                delayBeforeRes = getResolver(WritersManager.DelayBefore, writer.getValues());
            }
            if (delayBeforeRes==null) {
                log.warn("writeDelayBefore resolver not available");
                return;
            }
            log.debug("writing delayBefore=" + i);
            characteristic.setValue(delayBeforeRes.getValue(i));
            writeCharacteristic(deviceWrapper, characteristic);
        }

        private void writeMeasurementAvg(Characteristic characteristic, int i) {
            if (characteristic==null) {
                log.warn("writeMeasurementAvg char=null");
                return;
            }
            if (measurementAvgRes==null) {
                ValueWriter writer = WritersManager.getWriter(characteristic.getName());
                measurementAvgRes = getResolver(WritersManager.MeasurementAvg, writer.getValues());
            }
            if (measurementAvgRes==null) {
                log.warn("writeMeasurementAvg resolver not available");
                return;
            }
            log.debug("writing avg=" + i);
            characteristic.setValue(measurementAvgRes.getValue(i));
            writeCharacteristic(deviceWrapper, characteristic);
        }

        private void writeMeasurmentInterval(Characteristic characteristic, int i) {
            if (characteristic==null) {
                log.warn("writeMeasurementInterval char=null");
                return;
            }
            if (measurementIntervalRes==null) {
                ValueWriter writer = WritersManager.getWriter(characteristic.getName());
                measurementIntervalRes = getResolver(WritersManager.MeasurementInterval, writer.getValues());
            }
            if (measurementIntervalRes==null) {
                log.warn("writeMeasurementInterval resolver not available");
                return;
            }
            log.debug("writing interval=" + i);
            characteristic.setValue(measurementIntervalRes.getValue(i));
            writeCharacteristic(deviceWrapper, characteristic);
        }

        private void readCommand(Characteristic characteristic) {
            readCharacteristic(deviceWrapper, characteristic.getCharacteristic());
        }

        private void readLedDone(Characteristic characteristic) {
            log.debug("readLedDone");
            readCharacteristic(deviceWrapper, characteristic.getCharacteristic());
        }

        private boolean isCommandFinished(Characteristic characteristic) {
            int intValue = ByteConverter.bytesToInt(characteristic.getValue());
            log.debug("isCommandFinished "+intValue);
            return intValue <= 0;
        }

        private boolean isLedsFinished(Characteristic characteristic) {
            int intValue = ByteConverter.bytesToInt(characteristic.getValue());
            log.debug("isLedsFinished "+intValue);
            return intValue >=9;
        }

        private void readAllLeds() {

            for (Characteristic led : ledChars) {
                readCharacteristic(deviceWrapper, led.getCharacteristic());
            }
        }

        private void readLed(Characteristic ledChar) {
            LedReader reader = (LedReader) ReadersManager.getReader("Led");
            reader.read(ledChar);
            Led led = new Led(reader.getPDs());
            leds.add(led);
        }

        private ValueWriterResolver getResolver(String name, List<ValueWriterResolver> resolvers) {
            for (ValueWriterResolver resolver : resolvers) {
                if (resolver.getName().equals(name)) {
                    return resolver;
                }
            }
            return null;
        }
    }
}
