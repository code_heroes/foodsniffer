package com.trustfood.trustfood.view;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.trustfood.trustfood.BluetoothLeService;
import com.trustfood.trustfood.R;

public class CartridgeData extends Activity {

    public static final String SAMPLE_ASSAY = "sample_assay";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartridge_data);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        final TextView tv = (TextView) findViewById(R.id.sample_type);

        Button btn = (Button) findViewById(R.id.button_start_userdata);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CartridgeData.this, UserData.class);
                intent.putExtra(BluetoothLeService.DEVICE_ADDRESS, getIntent().getStringExtra(BluetoothLeService.DEVICE_ADDRESS));
                intent.putExtra(SAMPLE_ASSAY, tv.getText().toString());

                startActivity(intent);
            }
        });

        if (savedInstanceState != null) {
            tv.setText(savedInstanceState.getString(SAMPLE_ASSAY));
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        TextView tv = (TextView) findViewById(R.id.sample_type);
        outState.putString(SAMPLE_ASSAY, tv.getText().toString());
    }
}
