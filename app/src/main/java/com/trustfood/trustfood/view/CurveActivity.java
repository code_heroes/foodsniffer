package com.trustfood.trustfood.view;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;

import com.trustfood.trustfood.Dialogs;
import com.trustfood.trustfood.R;
import com.trustfood.trustfood.model.Led;
import com.trustfood.trustfood.utils.TempSaver;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 22.11.2014.
 */
public class CurveActivity extends Activity {
    private final Logger log = Logger.getLogger(CurveActivity.class.getSimpleName());

    List<List<Led>> measurementsSet=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getIntent()!=null) {
            measurementsSet = TempSaver.getMeasurements(getIntent().getExtras());
        }
        if(savedInstanceState!=null){
            measurementsSet = TempSaver.getMeasurements(savedInstanceState);
        }
        analyze();
        setContentView(R.layout.activity_frame);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        CurveListFragment fr=CurveListFragment.getInstance(measurementsSet);
        changeFragment(fr);
    }

    private void analyze(){
        for(List<Led> measurement:measurementsSet){
            for(Led led:measurement){
                for(Float value:led.getPds()){
                    if(value==0){
                        log.debug("measurement="+measurementsSet.indexOf(measurement)+
                        ",Led="+measurement.indexOf(led)+",PD="+led.getPds().indexOf(value));
                    }
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        int count=getFragmentManager().getBackStackEntryCount();
        if(count==1){
            Dialogs.showAlert("Warning","Are you sure ? All data will be lost",this,"Yes","No",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            },new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
        }
        else {
            super.onBackPressed();
        }
    }

    public void changeFragment(Fragment fragment){
        FragmentTransaction tr=getFragmentManager().beginTransaction();
        tr.replace(R.id.content_frame,fragment,"content");
        tr.addToBackStack(null);
        tr.commitAllowingStateLoss();
        getFragmentManager().executePendingTransactions();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(measurementsSet!=null) {
            TempSaver.putMeasurements(outState, measurementsSet);
        }
    }
}
