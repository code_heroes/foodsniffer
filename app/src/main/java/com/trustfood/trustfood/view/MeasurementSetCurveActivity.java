package com.trustfood.trustfood.view;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MenuItem;

import com.trustfood.trustfood.BluetoothLeService;
import com.trustfood.trustfood.DeviceManager;
import com.trustfood.trustfood.R;
import com.trustfood.trustfood.math.CurveUtils;
import com.trustfood.trustfood.model.DeviceWrapper;
import com.trustfood.trustfood.model.Led;
import com.trustfood.trustfood.utils.TempSaver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 28.01.2015.
 */
public class MeasurementSetCurveActivity extends Activity implements BluetoothLeService.MeasurementListener {
    DeviceWrapper mDeviceWrapper;
    BluetoothLeService mBluetoothLeService;
    ProgressListener listener;
    StatusListener statusListener;
    List<List<Led>> measurementsSet;
    boolean ready;
    public void setListener(ProgressListener listener) {
        this.listener = listener;
    }

    public void setStatusListener(StatusListener statusListener) {
        this.statusListener = statusListener;
        if(ready&&statusListener!=null){
            statusListener.onReady();
        }
    }

    public interface ProgressListener{
        void onProgressChanged(int current,int max);
    }

    public interface StatusListener {
        void onReady();
        void onDisconnected();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        String address = getIntent().getStringExtra(BluetoothLeService.DEVICE_ADDRESS);
        mDeviceWrapper = DeviceManager.getInstance().getDevice(address);
        registerReceiver(mBluetoothLeReceiver, getGattUpdateIntentFilter());
        connectService();
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                finish();
            }
            if(!mDeviceWrapper.isConnected()) {
                mBluetoothLeService.connect(mDeviceWrapper);
            }
            else if(!mBluetoothLeService.isMeasuringRunning()){
                mDeviceWrapper.getGatt().discoverServices();
            }
            checkScreen();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onContextItemSelected(item);
    }

    private void checkScreen(){
        if(mBluetoothLeService!=null){
            if(mBluetoothLeService.isMeasuringRunning()){
                ProgressFragment fr=new ProgressFragment();
                changeFragment(fr);
            }
            else {
                StartMeasureFragment fr=new StartMeasureFragment();
                changeFragment(fr);
            }
        }
    }

    private void connectService() {
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    private void disconnect() {
        if(mBluetoothLeService!=null) {
            unbindService(mServiceConnection);
        }
    }

    private final BroadcastReceiver mBluetoothLeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            switch (action) {

                case BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED:
                    ready=true;
                    if(statusListener!=null){
                        statusListener.onReady();
                    }
                    break;
                case BluetoothLeService.ACTION_GATT_CONNECTED:
                    mDeviceWrapper.getGatt().discoverServices();
                    break;
                case BluetoothLeService.ACTION_GATT_DISCONNECTED:
                    finish();
                    break;
                case BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR:
                    finish();
                    break;

            }
        }
    };

    private static IntentFilter getGattUpdateIntentFilter() {
        IntentFilter bleIntentFilter = new IntentFilter();
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR);

        return bleIntentFilter;
    }

    @Override
    public void onFinished(List<List<Led>> measurementsSet) {
       // Dialogs.showAlert(null, "Finished", this, null, null, null, null);
        this.measurementsSet=measurementsSet;

        if(measurementsSet!=null){
         //   List<CurveUtils.Point> points=CurveUtils.getMinSetCurve(measurementsSet,0);
            openCurveActivity();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBluetoothLeReceiver);
        disconnect();
    }

    @Override
    public void onProgressChanged(int current,int max) {
        Log.d("Progress","Progress: "+current);
        listener.onProgressChanged(current,max);
    }

    public void startMeasure(){
        if(mBluetoothLeService!=null){
            mBluetoothLeService.startReadAllLeds(mDeviceWrapper, MeasurementSetCurveActivity.this,
                    MeasurementSetCurveActivity.this);
            checkScreen();
        }
    }

    public void stopMeasure(){
        if(mBluetoothLeService!=null){
            measurementsSet=mBluetoothLeService.stopMeasure();
            if(measurementsSet!=null){
                openCurveActivity();
            }
        }
    }

    private void openCurveActivity(){
        Intent intent=new Intent(this,CurveActivity.class);
        Bundle bundle=new Bundle();
        TempSaver.putMeasurements(bundle,measurementsSet);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    private void changeFragment(Fragment fragment){
        FragmentTransaction tr=getFragmentManager().beginTransaction();
        tr.replace(R.id.content_frame,fragment,"content");
        tr.commitAllowingStateLoss();
        getFragmentManager().executePendingTransactions();
    }
}
