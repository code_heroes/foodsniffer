package com.trustfood.trustfood.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.trustfood.trustfood.App;
import com.trustfood.trustfood.R;
import com.trustfood.trustfood.math.CurveUtils;
import com.trustfood.trustfood.model.Led;
import com.trustfood.trustfood.utils.GraphUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 04.02.2015.
 */
public abstract class AbstractCurveFragment extends Fragment {
    protected GraphView graph;
    protected List<List<Led>> measurementsSet;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setXAxisBoundsManual(true);

        graph.onDataChanged(false, false);
        graph.getGridLabelRenderer().setGridColor(App.getmContext().getResources().getColor(R.color.graph_grid_color));
        graph.getGridLabelRenderer().setVerticalLabelsColor(App.getmContext().getResources().getColor(R.color.graph_text_color));
        graph.getGridLabelRenderer().setHorizontalLabelsColor(App.getmContext().getResources().getColor(R.color.graph_text_color));
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(){
            @Override
            public String formatLabel(double value, boolean isValueX) {
                return String.format("%d",(int)value);
            }
        });
        graph.getViewport().setScrollable(true);
    }


}
