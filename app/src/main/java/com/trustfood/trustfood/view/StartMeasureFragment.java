package com.trustfood.trustfood.view;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.trustfood.trustfood.R;

/**
 * Created by User-PC on 03.02.2015.
 */
public class StartMeasureFragment extends Fragment implements MeasurementSetCurveActivity.StatusListener{
    Button btnStartMeasure;
    MeasurementSetCurveActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_start_measure, container, false);
        btnStartMeasure = (Button) view.findViewById(R.id.btn_start_measure);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (MeasurementSetCurveActivity) getActivity();
        activity.setStatusListener(this);
        btnStartMeasure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startMeasure();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        activity.setStatusListener(null);
    }

    @Override
    public void onReady() {
        btnStartMeasure.setEnabled(true);
        btnStartMeasure.setText(R.string.start_measure);
    }

    @Override
    public void onDisconnected() {

    }
}