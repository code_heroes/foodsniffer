/*
 * Bluegiga’s Bluetooth Smart Android SW for Bluegiga BLE modules
 * Contact: support@bluegiga.com.
 *
 * This is free software distributed under the terms of the MIT license reproduced below.
 *
 * Copyright (c) 2013, Bluegiga Technologies
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files ("Software")
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
 * ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR PURPOSE.
 */
package com.trustfood.trustfood.view;

import java.io.File;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.trustfood.trustfood.BluetoothLeService;
import com.trustfood.trustfood.DeviceManager;
import com.trustfood.trustfood.Dialogs;
import com.trustfood.trustfood.Engine;
import com.trustfood.trustfood.Logger;
import com.trustfood.trustfood.R;
import com.trustfood.trustfood.adapter.DeviceAdapter;
import com.trustfood.trustfood.model.DeviceWrapper;
import com.trustfood.trustfood.qr.ValuesActivity;
import com.trustfood.trustfood.utils.Hex;
import com.trustfood.trustfood.writers.WritersManager;


public class MainActivity extends Activity {

    private static final int ACTIVITY_QR_CODE = 1;

    private static final int BlUETOOTH_SETTINGS_REQUEST_CODE = 100;
    public static final int SCAN_PERIOD = 20000;

    private static IntentFilter bleIntentFilter;
    private BluetoothLeService mBluetoothLeService;
    private Dialog mDialog;
    private boolean bleIsSupported = true;
    ImageView mBtnScan;
    List<DeviceWrapper> devices;
    DeviceAdapter adapter;


    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                finish();
            }
            startScanning();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    public void showEditAlert(){
        AlertDialog.Builder ab=new AlertDialog.Builder(this);
        ab.setTitle("Please provide description of the assay");
        final EditText input = new EditText(MainActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        ab.setView(input);
        ab.setPositiveButton("Ok", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SharedPreferences pf = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                pf.edit().putString("comment",input.getText().toString()).apply();
            }
        });

        ab.show();
    }

    private void refreshViewOnUiThread() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    private final BroadcastReceiver mBluetoothLeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (BluetoothLeService.ACTION_DEVICE_DISCOVERED.equals(action)) {
                adapter.notifyDataSetChanged();
//            } else if (intent.getAction().equals(BluetoothLeService.ACTION_START_SCAN)) {
//                mBtnScan.setChecked(true);
//            } else if (intent.getAction().equals(BluetoothLeService.ACTION_STOP_SCAN)) {
//                mBtnScan.setChecked(false);
            } else if (intent.getAction().equals(BluetoothLeService.ACTION_GATT_CONNECTED)) {
                //  refreshViewOnUiThread();
                //startServiceCharacteristicAtivity(intent.getStringExtra(BluetoothLeService.DEVICE_ADDRESS));
            } else if (intent.getAction().equals(BluetoothLeService.ACTION_GATT_DISCONNECTED)
                    || intent.getAction().equals(BluetoothLeService.ACTION_READ_REMOTE_RSSI)
                    || intent.getAction().equals(BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR)) {

                refreshViewOnUiThread();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Engine.init();
        //mBtnScan.setChecked(true);
        setContentView(R.layout.activity_main);
        try {
            String version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            TextView txtAppVersion = (TextView) findViewById(R.id.txt_app_version);
            txtAppVersion.setText(version);
        } catch (PackageManager.NameNotFoundException ex) {
        }


        mBtnScan = (ImageView) findViewById(R.id.bluetoothlogo);
        mBtnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mBluetoothLeService.isScanning()) {
                    Log.d("MainActivity", "manually starting bluetooth again");
                    mBluetoothLeService.startScanning(SCAN_PERIOD);
                }
            }
        });
        devices = DeviceManager.getInstance().getDevices();
        ListView listView = (ListView) findViewById(R.id.list_devices);
        adapter = new DeviceAdapter(this, -1, devices);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DeviceWrapper deviceWrapper = devices.get(i);
                //mBluetoothLeService.connect(deviceWrapper);
                showListModesDialog(deviceWrapper);
//                showQRCodeScreen(deviceWrapper);

            }
        });
     //   test();
        // Check if Bluetooth Low Energy technology is supported on device
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            bleIsSupported = false;

            mDialog = Dialogs.showAlert(this.getText(R.string.app_name), this.getText(R.string.ble_not_supported),
                    this, getText(android.R.string.ok), null, new OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int id) {
                            mDialog.dismiss();
                            finish();
                        }
                    }, null);
            return;
        }
//        showEditAlert();
        checkBluetoothAdapter();



    }

    private void showQRCodeScreen(DeviceWrapper deviceWrapper) {
        Intent intent = new Intent(this, ScanQrCode.class);
        intent.putExtra(BluetoothLeService.DEVICE_ADDRESS, deviceWrapper.getAddress());

        startActivity(intent);
    }

    private void test() {
        byte[] bytes = null;
        int valueS=15;
        bytes = ByteBuffer.allocate(4).putInt(valueS).array();
        String value = Hex.encodeHexString(bytes);

        byte[] result=hexStringToByteArray(value,4);
        Log.d("App", "Value " + value);
    }


    public static byte[] hexStringToByteArray(String s, int minBytesCount) {
        byte[] data = null;
        try {
            data = Hex.decodeHex(s.toCharArray());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
//        int len = s.length();
//        byte[] data = new byte[len / 2];
//        for (int i = 0; i < len; i += 2) {
//            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
//                    + Character.digit(s.charAt(i+1), 16));
//        }
        if (data.length < minBytesCount) {
            int blankBytes = minBytesCount - data.length;
            byte[] result = new byte[minBytesCount];
            for (int i = 0; i < data.length; ++i) {
                result[blankBytes + i - 1] = data[i];
            }
            return result;
        }
        return data;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.sensor_settings:
                sensor_settings();
                return true;
            case R.id.start_userflow:
                Intent intent = new Intent(this, ScanQrCode.class);
                intent.putExtra(BluetoothLeService.DEVICE_ADDRESS, "00:00:00:00:00:00");
                startActivity(intent);
                return true;
            case R.id.send_logs:
                sendLogsViaEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void restoreSensorSettings(Dialog dialog) {
        SharedPreferences pref=getSharedPreferences(BluetoothLeService.FoodSnifferPrefs,Context.MODE_PRIVATE);
        // update settings in view
        EditText textCurrentValue=(EditText)dialog.findViewById(R.id.etCurrentValue);
        textCurrentValue.setText(""+pref.getInt(WritersManager.CurrentValue,0));
        EditText textDelayBefore=(EditText)dialog.findViewById(R.id.etDelayBefore);
        textDelayBefore.setText(""+pref.getInt(WritersManager.DelayBefore,150));
        EditText textDelayAfter=(EditText)dialog.findViewById(R.id.etDelayAfter);
        textDelayAfter.setText(""+pref.getInt(WritersManager.DelayAfter,150));
        EditText textMeasuresAvg=(EditText)dialog.findViewById(R.id.etMeasuresAvg);
        textMeasuresAvg.setText(""+pref.getInt(WritersManager.MeasurementAvg,10));;
        EditText textMeasureInterval=(EditText)dialog.findViewById(R.id.etMeasureInterval);
        textMeasureInterval.setText(""+pref.getInt(WritersManager.MeasurementInterval,20));
        EditText textReadInterval=(EditText)dialog.findViewById(R.id.etReadInterval);
        textReadInterval.setText(""+pref.getInt(BluetoothLeService.ReadInterval,50));
        CheckBox cbSendAvgSettings=(CheckBox)dialog.findViewById(R.id.cbSendAvgSettings);
        cbSendAvgSettings.setChecked(pref.getBoolean(BluetoothLeService.SendAvgSettings,true));
    }

    void storeSensorSettings(Dialog dialog) {
        SharedPreferences pref=getSharedPreferences(BluetoothLeService.FoodSnifferPrefs,Context.MODE_PRIVATE);
        SharedPreferences.Editor epref=pref.edit();
        EditText textCurrentValue=(EditText)dialog.findViewById(R.id.etCurrentValue);
        int value=Integer.parseInt(textCurrentValue.getText().toString());
        epref.putInt(WritersManager.CurrentValue,value);
        EditText textDelayBefore=(EditText)dialog.findViewById(R.id.etDelayBefore);
        value=Integer.parseInt(textDelayBefore.getText().toString());
        epref.putInt(WritersManager.DelayBefore,value);
        EditText textDelayAfter=(EditText)dialog.findViewById(R.id.etDelayAfter);
        value=Integer.parseInt(textDelayAfter.getText().toString());
        epref.putInt(WritersManager.DelayAfter,value);
        EditText textMeasuresAvg=(EditText)dialog.findViewById(R.id.etMeasuresAvg);
        value=Integer.parseInt(textMeasuresAvg.getText().toString());
        epref.putInt(WritersManager.MeasurementAvg,value);
        EditText textMeasureInterval=(EditText)dialog.findViewById(R.id.etMeasureInterval);
        value=Integer.parseInt(textMeasureInterval.getText().toString());
        epref.putInt(WritersManager.MeasurementInterval,value);
        EditText textReadInterval=(EditText)dialog.findViewById(R.id.etReadInterval);
        value=Integer.parseInt(textReadInterval.getText().toString());
        epref.putInt(BluetoothLeService.ReadInterval,value);
        CheckBox cbSendAvgSettings=(CheckBox)dialog.findViewById(R.id.cbSendAvgSettings);
        epref.putBoolean(BluetoothLeService.SendAvgSettings,cbSendAvgSettings.isChecked());

        epref.commit();
    }

    public boolean sensor_settings() {
        final AlertDialog.Builder builder=new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        builder.setView(li.inflate(R.layout.sensor_settings, null)).
                setTitle("Sensor settings").setInverseBackgroundForced(true);
        builder.setPositiveButton("Apply",new OnClickListener() {
            public void onClick (DialogInterface d,int i){
                storeSensorSettings((Dialog)d);
                d.dismiss();
            }
         })
         .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            public void onClick (DialogInterface d,int i){
                restoreSensorSettings((Dialog)d);
                d.cancel();
            }
          });
        /*
         .setNeutralButton("Revert", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface d, int i) {
                 restoreSensorSettings((Dialog)d);
             }
         });
         */

        AlertDialog dialog=builder.create();
        dialog.show();
        restoreSensorSettings(dialog);

		return true;
    }

    // Called after back button click while connection was establishing
    private void restartBluetooth() {
        mBluetoothLeService.close();
        startScanning();
    }

    private void sendLogsViaEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"debug@trustfood.org"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "foodsniffer_debug");
        SharedPreferences pf = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        String comment=pf.getString("comment", "no comment");;
        intent.putExtra(Intent.EXTRA_TEXT,comment);
        File file = Logger.getLogFile();
        Uri uri = Uri.fromFile(file);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(intent, "Send email..."));
    }

    private void checkBluetoothAdapter() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            bluetoothNotSupported();
        } else if (!bluetoothAdapter.isEnabled()) {
            bluetoothEnable();
        } else {
            connectService();
        }
    }

    private void connectService() {
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    // Displays dialog with information that phone doesn't support Bluetooth
    private void bluetoothNotSupported() {
        mDialog = Dialogs.showAlert(getText(R.string.app_name), getText(R.string.bluetooth_not_supported), this,
                getText(android.R.string.ok), null, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }, null);
    }


    private void startScanning() {
        registerReceiver(mBluetoothLeReceiver, getGattUpdateIntentFilter());

        mBluetoothLeService.startScanning(SCAN_PERIOD);
    }

    private static IntentFilter getGattUpdateIntentFilter() {
        if (bleIntentFilter == null) {
            bleIntentFilter = new IntentFilter();
            bleIntentFilter.addAction(BluetoothLeService.ACTION_DEVICE_DISCOVERED);
            bleIntentFilter.addAction(BluetoothLeService.ACTION_START_SCAN);
            bleIntentFilter.addAction(BluetoothLeService.ACTION_STOP_SCAN);
            // bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
            bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
            bleIntentFilter.addAction(BluetoothLeService.ACTION_READ_REMOTE_RSSI);
            bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR);
        }
        return bleIntentFilter;
    }


    // Displays dialog and request user to enable Bluetooth
    private void bluetoothEnable() {
        mDialog = Dialogs.showAlert(this.getText(R.string.no_bluetooth_dialog_title_text), this
                        .getText(R.string.no_bluetooth_dialog_text), this, getText(android.R.string.ok),
                getText(android.R.string.cancel), new OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        Intent intentBluetooth = new Intent();
                        intentBluetooth.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                        MainActivity.this.startActivityForResult(intentBluetooth, BlUETOOTH_SETTINGS_REQUEST_CODE);
                    }
                }, new OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        MainActivity.this.finish();
                    }
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BlUETOOTH_SETTINGS_REQUEST_CODE) {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (!bluetoothAdapter.isEnabled() && mDialog != null) {
                mDialog.show();
            } else {
                connectService();
            }
        }
        if (requestCode == ACTIVITY_QR_CODE) {
            if (resultCode == RESULT_OK) {
                showQRcodeInfo(data.getSerializableExtra("data"));
            }

        }
    }

    public void showQRcodeInfo(Serializable values) {
        if (values != null) {
            Intent intent = new Intent(this, ValuesActivity.class);
            intent.putExtra(ValuesActivity.PARAM_VALUES, values);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Wrong format", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bleIsSupported) {
            registerReceiver(mBluetoothLeReceiver, getGattUpdateIntentFilter());
            if (mBluetoothLeService != null) {
                //setScanningProgress(mBluetoothLeService.isScanning());
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (bleIsSupported) {
            unregisterReceiver(mBluetoothLeReceiver);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBluetoothLeService != null) {
            mBluetoothLeService.close();
        }
        //DeviceManager.getInstance().disconnectAll();
        if (bleIsSupported && mBluetoothLeService != null) {
            unbindService(mServiceConnection);
        }
        mBluetoothLeService = null;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mBluetoothLeService != null && mBluetoothLeService.isScanning()) {
                mBluetoothLeService.close();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {

        if (mBluetoothLeService != null && mBluetoothLeService.isScanning()) {
            mBluetoothLeService.close();
        } else {
            super.onBackPressed(); // allows standard use of backbutton for page
            // 1
        }
    }

    public void showListModesDialog(final DeviceWrapper deviceWrapper) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                this);
        builderSingle.setTitle("Select Mode");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add(getString(R.string.dlg_lbl_manual));
        arrayAdapter.add(getString(R.string.dlg_lbl_commence_assay));
        builderSingle.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = null;
                        switch (which) {
                            case 0:
                                intent = new Intent(MainActivity.this, DeviceActivity.class);
                                break;
                            case 1:
                                intent = new Intent(MainActivity.this, ScanQrCode.class);
                                break;
                        }
                        intent.putExtra(BluetoothLeService.DEVICE_ADDRESS, deviceWrapper.getAddress());
                        startActivity(intent);
                    }
                });
        builderSingle.show();
    }
}
