package com.trustfood.trustfood.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.trustfood.trustfood.BluetoothLeService;
import com.trustfood.trustfood.R;

public class ScanQrCode extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr_code);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        Button btn = (Button) findViewById(R.id.button_OK);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScanQrCode.this, CartridgeData.class);
                intent.putExtra(BluetoothLeService.DEVICE_ADDRESS, getIntent().getStringExtra(BluetoothLeService.DEVICE_ADDRESS));

                startActivity(intent);
            }
        });
    }
}
