package com.trustfood.trustfood.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.trustfood.trustfood.BluetoothLeService;
import com.trustfood.trustfood.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class UserData extends Activity {

    public static final String LOC_SAMPLE = "loc_sample";
    public static final String REFERENCE_NO = "reference_no";
    public static final String SAMPLE_DESCRIPTION = "sample_description";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        Spinner spinner = (Spinner) findViewById(R.id.spinner_operator);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.operator_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner = (Spinner) findViewById(R.id.spinner_sample_prep);
        adapter = ArrayAdapter.createFromResource(this,
                R.array.sample_prep_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        TextView tv;
        if (savedInstanceState != null) {
            tv = (TextView) findViewById(R.id.tv_location_sample);
            tv.setText(savedInstanceState.getString(LOC_SAMPLE));
            tv = ((TextView) findViewById(R.id.tv_reference_no));
            tv.setText(savedInstanceState.getString(REFERENCE_NO));
            tv = ((TextView) findViewById(R.id.tv_sample_descr));
            tv.setText(savedInstanceState.getString(SAMPLE_DESCRIPTION));
        }

        tv = (TextView) findViewById(R.id.lbl_sample_assay_type);
        tv.setText(getIntent().getStringExtra(CartridgeData.SAMPLE_ASSAY));

        Button submitbtn = (Button) findViewById(R.id.btn_submit);
        submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserData.this, ApplySample.class);
                intent.putExtra(BluetoothLeService.DEVICE_ADDRESS, getIntent().getStringExtra(BluetoothLeService.DEVICE_ADDRESS));

                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.user_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }else if(item.getItemId() == R.id.share){

            try {

                Bitmap adv = BitmapFactory.decodeResource(getResources(), R.drawable.whatsapp_chart);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/jpeg");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                adv.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                File f = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "temporary_file.jpg");
                try {
                    f.createNewFile();
                    new FileOutputStream(f).write(bytes.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                share.putExtra(Intent.EXTRA_STREAM, Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg"));
                    share.setPackage("com.whatsapp");
                    startActivity(Intent.createChooser(share, "Deliver Analytical Results of the Assay by"));

            }catch(Exception e){
                Toast.makeText(UserData.this, "Unabe to share data right now", Toast.LENGTH_SHORT).show();
            }

        }
        return super.onContextItemSelected(item);
    }
    @Override

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        TextView tv = (TextView) findViewById(R.id.tv_location_sample);
        outState.putString(LOC_SAMPLE, tv.getText().toString());
        tv = ((TextView) findViewById(R.id.tv_reference_no));
        outState.putString(REFERENCE_NO, (String) tv.getText().toString());
        tv = ((TextView) findViewById(R.id.tv_sample_descr));
        outState.putString(SAMPLE_DESCRIPTION, (String) tv.getText().toString());
    }
}
