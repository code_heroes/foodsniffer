package com.trustfood.trustfood.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trustfood.trustfood.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class UserDataFragment extends Fragment {

    public UserDataFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_data, container, false);
    }
}
