package com.trustfood.trustfood.view;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.app.Activity;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.trustfood.trustfood.BluetoothLeService;
import com.trustfood.trustfood.DeviceManager;
import com.trustfood.trustfood.R;
import com.trustfood.trustfood.dialogs.MeasurementProgressDialog;
import com.trustfood.trustfood.model.DeviceWrapper;
import com.trustfood.trustfood.model.Led;
import com.trustfood.trustfood.utils.TempSaver;

import java.util.ArrayList;
import java.util.List;

public class SubmitResult extends Activity implements BluetoothLeService.MeasurementListener, MeasurementProgressDialog.MeasurementDialogListener {
    DeviceWrapper mDeviceWrapper;
    BluetoothLeService mBluetoothLeService;
    ProgressListener listener;
    StatusListener statusListener;
    private boolean ready;

    List<List<Led>> measurementsSet = new ArrayList<>();

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        stopMeasure();
    }

    public interface ProgressListener{
        void onProgressChanged(int current,int max);

    }
    public interface StatusListener {
        void onReady();
        void onDisconnected();

    }

    public void setStatusListener(StatusListener statusListener) {
        this.statusListener = statusListener;
        if(ready&&statusListener!=null){
            statusListener.onReady();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_result);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        String address = getIntent().getStringExtra(BluetoothLeService.DEVICE_ADDRESS);
        mDeviceWrapper = DeviceManager.getInstance().getDevice(address);
        registerReceiver(mBluetoothLeReceiver, getGattUpdateIntentFilter());
        connectService();

        Button btn = (Button) findViewById(R.id.btn_submit);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SubmitResult.this, "Result will be submitted here", Toast.LENGTH_SHORT).show();
            }
        });

        measurementsSet.add(new ArrayList<Led>());
        LedMinimumCurve fr=LedMinimumCurve.getInstance(measurementsSet);
        changeFragment(fr);
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                finish();
            }
            if(!mDeviceWrapper.isConnected()) {
                mBluetoothLeService.connect(mDeviceWrapper);
            }
            else if(!mBluetoothLeService.isMeasuringRunning()){
                mDeviceWrapper.getGatt().discoverServices();
            }
            startMeasure();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    private void connectService() {
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    public void onFinished(List<List<Led>> measurementsSet) {
        // Dialogs.showAlert(null, "Finished", this, null, null, null, null);
        this.measurementsSet=measurementsSet;

        if(measurementsSet!=null){
            //   List<CurveUtils.Point> points=CurveUtils.getMinSetCurve(measurementsSet,0);
            LedMinimumCurve fr=LedMinimumCurve.getInstance(measurementsSet);
            changeFragment(fr);;
        }
    }

    public void startMeasure(){
        if(mBluetoothLeService!=null){
            mBluetoothLeService.startReadAllLeds(mDeviceWrapper, SubmitResult.this,
                    SubmitResult.this);
            DialogFragment fragment = new MeasurementProgressDialog();
            fragment.show(getFragmentManager(), "Measurement Progress");
        }
    }

    public void stopMeasure(){
        if(mBluetoothLeService!=null){
            measurementsSet=mBluetoothLeService.stopMeasure();
            if(measurementsSet!=null){
                LedMinimumCurve fr=LedMinimumCurve.getInstance(measurementsSet);
                changeFragment(fr);;
            }
        }
    }

    private void checkScreen(){
        if(mBluetoothLeService!=null){
            if(mBluetoothLeService.isMeasuringRunning()){
                ProgressFragment fr=new ProgressFragment();
                changeFragment(fr);
            }
            else {
            }
        }
    }

    @Override
    public void onProgressChanged(int current, int max) {
        Log.d("Progress","Progress: "+current);
        listener.onProgressChanged(current,max);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBluetoothLeReceiver);
        disconnect();
    }

    private void disconnect(){
        if(mBluetoothLeService!=null) {
            unbindService(mServiceConnection);
        }
    }


    public void changeFragment(Fragment fragment){
        FragmentTransaction tr=getFragmentManager().beginTransaction();
        tr.replace(R.id.graph_fragment, fragment,"content");
        tr.addToBackStack(null);
        tr.commitAllowingStateLoss();
        getFragmentManager().executePendingTransactions();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(measurementsSet!=null) {
            TempSaver.putMeasurements(outState, measurementsSet);
        }
    }
    private final BroadcastReceiver mBluetoothLeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            switch (action) {

                case BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED:
                    ready=true;
                    if(statusListener!=null){
                        statusListener.onReady();
                    }
                    break;
                case BluetoothLeService.ACTION_GATT_CONNECTED:
                    mDeviceWrapper.getGatt().discoverServices();
                    break;
                case BluetoothLeService.ACTION_GATT_DISCONNECTED:
                    finish();
                    break;
                case BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR:
                    finish();
                    break;

            }
        }
    };

    private static IntentFilter getGattUpdateIntentFilter() {
        IntentFilter bleIntentFilter = new IntentFilter();
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR);

        return bleIntentFilter;
    }
}
