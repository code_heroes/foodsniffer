package com.trustfood.trustfood.view;

import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.trustfood.trustfood.R;
import com.trustfood.trustfood.model.Led;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 04.02.2015.
 */
public class CurveListFragment extends ListFragment {

    List<List<Led>> measurementsSet;

    public static CurveListFragment getInstance(List<List<Led>> measurementsSet){
        CurveListFragment fr=new CurveListFragment();
        fr.measurementsSet=measurementsSet;
        return fr;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        List<Fragment> fragments=new ArrayList<>();
        fragments.add(MeasurementCurve.getInstance(measurementsSet));
        fragments.add(LedMinimumCurve.getInstance(measurementsSet));

        ArrayAdapter<Fragment> adapter=new ArrayAdapter<Fragment>(getActivity(), R.layout.item_text,fragments);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Fragment fr=(Fragment)getListAdapter().getItem(position);
        CurveActivity activity=(CurveActivity)getActivity();
        activity.changeFragment(fr);
    }
}
