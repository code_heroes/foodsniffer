package com.trustfood.trustfood.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.trustfood.trustfood.App;
import com.trustfood.trustfood.R;
import com.trustfood.trustfood.math.CurveUtils;
import com.trustfood.trustfood.model.Led;
import com.trustfood.trustfood.utils.GraphUtils;
import com.trustfood.trustfood.utils.TempSaver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 04.02.2015.
 */
public class LedMinimumCurve extends AbstractCurveFragment {

    Spinner spinner;

    public static LedMinimumCurve getInstance(List<List<Led>> measurementsSet){
        LedMinimumCurve fr=new LedMinimumCurve();
        fr.measurementsSet=measurementsSet;
        return fr;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fr_led_minimum,container,false);
        graph = (GraphView) view.findViewById(R.id.graph);
        spinner=(Spinner)view.findViewById(R.id.spinner);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState!=null){
            measurementsSet = TempSaver.getMeasurements(savedInstanceState);
        } else {
            measurementsSet = new ArrayList<>();
        }
        List<String> leds=new ArrayList<>();
        leds.add("Assay 1 One OchraToxin A");

        for(int i=1;i<Led.LED_COUNT;++i){
            leds.add("Control Sensor "+String.valueOf(i+1));
        }
        ArrayAdapter<String> adapter=new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,leds);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                showSpecificLed(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner.setSelection(0);
    }

    private void showSpecificLed(int index){
        graph.removeAllSeries();
        List<CurveUtils.Point> points=CurveUtils.getMinSetCurve(measurementsSet,index);
        LineGraphSeries<DataPoint> series= GraphUtils.convert(points);

        graph.addSeries(series);
        float[] maxminY=GraphUtils.findMaxMinY(points, CurveUtils.Point.VALUE.Y);
        float[] maxminX=GraphUtils.findMaxMinY(points, CurveUtils.Point.VALUE.X);

        graph.getViewport().setMinY(maxminY[1] - maxminY[1] * 0.01f);
        graph.getViewport().setMaxY(maxminY[0] + maxminY[0] * 0.01f);
        graph.getViewport().setMinX(0);
        //float maxX=50;
//        if(maxminX[0]<50){
          float  maxX=maxminX[0]+maxminX[0]*.01f;
        //}
        graph.getViewport().setMaxX(maxX);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(measurementsSet!=null) {
            TempSaver.putMeasurements(outState, measurementsSet);
        }
    }

    @Override
    public String toString() {
        return "Wavelength";
    }
}
