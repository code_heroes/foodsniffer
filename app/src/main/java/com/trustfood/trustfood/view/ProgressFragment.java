package com.trustfood.trustfood.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trustfood.trustfood.BluetoothLeService;
import com.trustfood.trustfood.R;

/**
 * Created by User-PC on 03.02.2015.
 */
public class ProgressFragment extends Fragment implements MeasurementSetCurveActivity.ProgressListener {
    ProgressBar prProgress;
    TextView txtProgress;
    Button btnStop;
    MeasurementSetCurveActivity activity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fr_progress_bar,container,false);
        prProgress=(ProgressBar)view.findViewById(R.id.pr_progress);
        txtProgress=(TextView)view.findViewById(R.id.txt_progress);
        btnStop=(Button)view.findViewById(R.id.btn_stop);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity=(MeasurementSetCurveActivity)getActivity();
        activity.setListener(this);
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.stopMeasure();
            }
        });
        onProgressChanged(0, BluetoothLeService.MeasurementsReader.READS_COUNT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        activity.setListener(null);
    }

    @Override
    public void onProgressChanged(int current,int max) {

        txtProgress.setText("Measurement "+ current+" of "+max);
    }
}
