package com.trustfood.trustfood.view;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;

import com.trustfood.trustfood.BluetoothLeService;
import com.trustfood.trustfood.R;

public class ApplySample extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_sample);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        Button btn = (Button) findViewById(R.id.btn_sample_continue);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApplySample.this, SubmitResult.class);
                intent.putExtra(BluetoothLeService.DEVICE_ADDRESS, getIntent().getStringExtra(BluetoothLeService.DEVICE_ADDRESS));

                startActivity(intent);
            }
        });
    }

}
