package com.trustfood.trustfood.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.trustfood.trustfood.App;
import com.trustfood.trustfood.R;
import com.trustfood.trustfood.math.CurveUtils;
import com.trustfood.trustfood.model.Led;
import com.trustfood.trustfood.utils.GraphUtils;
import com.trustfood.trustfood.utils.TempSaver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 04.02.2015.
 */
public class MeasurementCurve extends AbstractCurveFragment {


    Spinner spinner;
    int[] colors=new int[]{R.color.color_1,R.color.color_2,R.color.color_3,R.color.color_4,
            R.color.color_5,R.color.color_6,R.color.color_7,R.color.color_8,R.color.color_9,
            R.color.color_10};

    public static MeasurementCurve getInstance(List<List<Led>> measurementsSet) {
        MeasurementCurve fr = new MeasurementCurve();
        fr.measurementsSet = measurementsSet;
        return fr;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_led_minimum, container, false);
        graph = (GraphView) view.findViewById(R.id.graph);
        spinner = (Spinner) view.findViewById(R.id.spinner);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            measurementsSet = TempSaver.getMeasurements(savedInstanceState);
        }
        List<String> data = new ArrayList<>();
        for (int i = 0; i < measurementsSet.size(); ++i) {
            data.add("Raw spectral data "+String.valueOf(i+1));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                graph.removeAllSeries();
                List<Led> measurement=measurementsSet.get(i);
                List<List<CurveUtils.Point>> points=CurveUtils.getMeasurement(measurement);
                int index=0;

                addLed(points.get(1),colors[index]);
//                for(List<CurveUtils.Point> led:points){
//                    addLed(led,colors[index]);
//                    ++index;
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner.setSelection(0);
    }

    private void addLed(List<CurveUtils.Point> points,int resColor) {

       LineGraphSeries<DataPoint> series = GraphUtils.convert(points);
        series.setDrawDataPoints(true);

        series.setColor(App.getmContext().getResources().getColor(resColor));
        graph.addSeries(series);
        float[] maxminY = GraphUtils.findMaxMinY(points, CurveUtils.Point.VALUE.Y);
        float[] maxminX = GraphUtils.findMaxMinY(points, CurveUtils.Point.VALUE.X);
        graph.getViewport().setMinY(-1);
        graph.getViewport().setMaxY(maxminY[0] + maxminY[0] * 0.05f);
        graph.getViewport().setMinX(maxminX[1] - maxminX[1] * 0.05f);
        float maxX = maxminX[0] + maxminX[0] * .05f;
        graph.getViewport().setMaxX(maxX);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (measurementsSet != null) {
            TempSaver.putMeasurements(outState, measurementsSet);
        }
    }

    @Override
    public String toString() {
        return "Assays performed";
    }
}
