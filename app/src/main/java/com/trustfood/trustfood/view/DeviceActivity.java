package com.trustfood.trustfood.view;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.trustfood.trustfood.BluetoothLeService;
import com.trustfood.trustfood.DeviceManager;
import com.trustfood.trustfood.R;
import com.trustfood.trustfood.adapter.ValuesAdapter;
import com.trustfood.trustfood.dialogs.WriteDialog;
import com.trustfood.trustfood.model.Characteristic;
import com.trustfood.trustfood.model.DeviceWrapper;
import com.trustfood.trustfood.readers.ReadersManager;
import com.trustfood.trustfood.readers.ValueReader;
import com.trustfood.trustfood.utils.ByteConverter;
import com.trustfood.trustfood.dialogs.LedDialog;

import org.apache.log4j.*;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by razir on 06.11.2014.
 */
public class DeviceActivity extends Activity implements ValuesAdapter.OperationListener,AdapterView.OnItemClickListener {
    private final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(BluetoothLeService.class.getSimpleName());
    DeviceWrapper mDeviceWrapper;
    BluetoothLeService mBluetoothLeService;
    ValuesAdapter adapter;
    List<Characteristic> values=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Engine.init();
        setContentView(R.layout.activity_device);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        ListView listChars=(ListView)findViewById(R.id.list_chars);
        listChars.setOnItemClickListener(this);
        adapter=new ValuesAdapter(this,values,this);
        listChars.setAdapter(adapter);
        String address = getIntent().getStringExtra(BluetoothLeService.DEVICE_ADDRESS);
        mDeviceWrapper = DeviceManager.getInstance().getDevice(address);
        registerReceiver(mBluetoothLeReceiver, getGattUpdateIntentFilter());
        connectService();



//        Characteristic characteristic3=Engine.getCharacteristic(UUID.fromString("d97095b8-ddb9-419f-b7d8-e1a9c59608ff"));
//
//        int[] data =new int[] {0x23, 0xd1 };
//        byte[] post=integersToBytes(data);
//        characteristic3.setValue(post);
//        Characteristic characteristic=Engine.getCharacteristic(UUID.fromString("42f649dc-6ecd-4fbf-97ab-c7da99fadc14"));
//        Characteristic characteristic2=Engine.getCharacteristic(UUID.fromString("e08d413b-b6e3-4c86-9b08-7e64955e7c2f"));
//        updateCharacteristic(characteristic);
//        characteristic2.setValue("something".getBytes());
//        updateCharacteristic(characteristic2);
//        updateCharacteristic(characteristic3);


        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.device_activity_menu, menu);
        return true;
    }

    byte[] integersToBytes(int[] values) {

        ByteBuffer byteBuffer = ByteBuffer.allocate(values.length * 4);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        intBuffer.put(values);

        byte[] array = byteBuffer.array();
        return array;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }else if(item.getItemId() == R.id.refresh_list){
            if((mBluetoothLeService != null) && (mDeviceWrapper != null)){
                mBluetoothLeService.connect(mDeviceWrapper);
                mBluetoothLeService.readAllCharacteristics(mDeviceWrapper);
            }
        }
        return super.onContextItemSelected(item);
    }

    private void showLedScreen(Characteristic characteristic) {
        DialogFragment dialog = LedDialog.getInstance(characteristic);
        dialog.show(getFragmentManager(), null);
    }

    private void showWriteScreen(Characteristic characteristic) {
        DialogFragment dialog = WriteDialog.getInstance(characteristic, mBluetoothLeService, mDeviceWrapper);
        dialog.show(getFragmentManager(), null);
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                finish();
            }
            mBluetoothLeService.connect(mDeviceWrapper);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    private void connectService() {
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }


    private final BroadcastReceiver mBluetoothLeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            log.debug("action-" + action);
            switch (action) {
                case BluetoothLeService.ACTION_DATA_WRITE:
                    mBluetoothLeService.readAllCharacteristics(mDeviceWrapper);
                    break;
                case BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED:
                    fillAllCharacteristics();
                    break;
                case BluetoothLeService.ACTION_GATT_CONNECTED:
                    mDeviceWrapper.getGatt().discoverServices();
                    break;
                case BluetoothLeService.ACTION_GATT_DISCONNECTED:
                    finish();
                    break;
                case BluetoothLeService.ACTION_DATA_AVAILABLE:
                    String uuid = intent.getStringExtra(BluetoothLeService.UUID_CHARACTERISTIC);
                    Characteristic characteristic = mDeviceWrapper.getCharacteristic(uuid);
                    adapter.updateCharacteristic(characteristic);
                    break;
            }
        }
    };

    private void fillAllCharacteristics() {
        if (mDeviceWrapper.getCharacteristics() != null) {
            values.clear();
            values.addAll(mDeviceWrapper.getCharacteristics());
            adapter.notifyDataSetChanged();
        }
    }

    private static IntentFilter getGattUpdateIntentFilter() {

        IntentFilter bleIntentFilter = new IntentFilter();
        bleIntentFilter.addAction(BluetoothLeService.ACTION_DEVICE_DISCOVERED);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_DATA_WRITE);

        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        //bleIntentFilter.addAction(BluetoothLeService.ACTION_READ_REMOTE_RSSI);
        bleIntentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR);

        return bleIntentFilter;
    }

    @Override
    public void onReadNeeds(Characteristic characteristic) {
        mBluetoothLeService.readCharacteristic(mDeviceWrapper, characteristic.getCharacteristic());
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Characteristic characteristic=(Characteristic)adapterView.getItemAtPosition(i);
        switch (characteristic.getGroup()){
            case Characteristic.GROUP_INFO:
                break;
            case Characteristic.GROUP_LED:
                showLedScreen(characteristic);
                break;
            case Characteristic.GROUP_WRITE:
                showWriteScreen(characteristic);
                break;
        }
    }
}
